////////////////////////////////////////////////////////////////////////////
//                              PARSER
//
// Name  : parser for the T language compiler
// Phase : 4
// Date  : May 2013
// Author: Xiaobo Sun
//
// Discription:
//  If the preprocessor symbol YYDEBUG is defined then the parser
//  can be built in stand-alone mode, to allow the parser to be
//  tested independently.
//
//  See the Makefile target "parsedbg" for how to build the stand-alone
//  parser.
//
//  To run it: % parsedbg <input
//
//  It will read the tokens parse the tokens one by one.
////////////////////////////////////////////////////////////////////////////

%{

#include <iostream>
using namespace std;

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "AST.h"
#include "Type.h"

// this routine is in scan.ll
int getCurrentSourceLineNumber();

// type module is in main.cxx
extern TypeModule* types;

void yyerror(const char *);
extern int yyparse(void);
extern int yylex(void);

extern bool dumpBefore;
extern bool dumpAfter;
extern bool dumpClasses;

%}

%union {
        unsigned int value;
        char* str;
        AST_Node* node;
        AST_MainBlock* mainBlock;
        AST_StatementList* statementList;
        AST_Statement* statement;
        AST_Expression* expression;
        Type* type;
        AST_IntegerLiteral* intLiteral;
        AST_IDList* idList;
        AST_ID* id;
        AST_ClassDecl* classDecl;
        AST_List* list;
        AST_Declaration* declaration;
};

%token <str> IDENTIFIER
%token <value> INTEGER_LITERAL
%token EQ_OP

%token INT
%token MAIN CLASS EXTENDS NEW DELETE
%token THIS SUPER
%token IF ELSE WHILE RETURN
%token OUT
%token BREAK CONTINUE
%token NULL_LITERAL

%token '/'
%token '='
%token ';'
%token '('
%token ')'
%token BAD

%type <list> Program
%type <list> CompilationUnit
%type <mainBlock> MainFunctionDeclaration
%type <mainBlock> MainFunctionBody
%type <idList> VariableDeclarators
%type <id> VariableDeclarator
%type <statement> BlockStatement
%type <statementList> MainBlock
%type <statementList> MainBlockStatements
%type <statement> MainBlockStatement
%type <statement> MainVariableDeclarationStatement
%type <statement> MainVariableDeclaration
%type <statement> Statement
%type <statement> OutputStatement
%type <statement> ExpressionStatement
%type <expression> ParenExpression
%type <expression> StatementExpression
%type <expression> Expression
%type <expression> AssignmentExpression
%type <expression> Assignment
%type <expression> LeftHandSide
%type <expression> EqualityExpression
%type <expression> RelationalExpression
%type <expression> AdditiveExpression
%type <expression> MultiplicativeExpression
%type <expression> UnaryExpression
%type <expression> CastExpression
%type <expression> Primary
%type <expression> PrimaryNoNewArray
%type <type> Type
%type <type> PrimitiveType
%type <type> NumericType
%type <type> IntegralType
%type <str> Identifier
%type <expression> Literal
%type <str> VariableDeclaratorID

%type <statementList> BlockStatements
%type <statement> Block
%type <statement> EmptyStatement
%type <statement> IfThenElseStatement
%type <statement> WhileStatement
%type <statement> ReturnStatement
%type <statement> BreakStatement
%type <statement> ContinueStatement
%type <statement> DeleteStatement

%type <list> ClassDeclarations
%type <classDecl> ClassDeclaration
%type <list> ClassBody
%type <list> ClassBodyDeclarations
%type <declaration> ClassBodyDeclaration
%type <declaration> ClassMemberDeclaration
%type <declaration> ConstructorDeclaration
%type <declaration> DestructorDeclaration
%type <declaration> FieldDeclaration
%type <declaration> MethodDeclaration
%type <declaration> MethodDeclarator
%type <declaration> ConstructorDeclarator
%type <list> ConstructorBody
%type <str> DestructorDeclarator
%type <expression> ClassInstanceCreationExpression
%type <expression> FieldAccess
%type <expression> MethodInvocation
%type <expression> ConstructorInvocation
%type <statement> MethodBody
%type <list> DestructorBody
%type <list> FormalParameters
%type <list> FormalParameterList
%type <declaration> FormalParameter
%type <type> ReferenceType
%type <type> ClassType
%type <list> Arguments
%type <list> ArgumentList



%start Program

%%

//
Program
  :CompilationUnit
  {
    $$ = $1;
    if(dumpBefore)
    {
      cerr << endl;
      $$->dump();
      cerr << endl;
    }
    //do preAnalyze1
    $$->preAnalyze1();
    
    //do preAnalyze2
    $$->preAnalyze2();
    
    //update class info(create VMT)
    types->updateInfo();
   
    // do semantic analysis
    $$->analyze();

    if(dumpClasses)
    {
      types->dumpClasses();
    }
    
    //if error found
    if($$->errCount > 0)
    {
      cerr << endl;
      cerr << $$->errCount << " semantic errors found!" << endl;
    }
    
    // dump results for debugging
    if(dumpAfter)
    {
      cerr << endl;
      $$->dump();
      cerr << endl;
    }
    
    // generate code
    if($$->errCount == 0)
    {
      $$->encode();
    }

    // cleanup the AST
    delete $$;
  };

//
CompilationUnit
	: MainFunctionDeclaration
  {
    $$ = new AST_List($1, NULL);
   
  }
  | MainFunctionDeclaration ClassDeclarations
  {
    $$ = new AST_List($1, $2);
  }
	| ClassDeclarations MainFunctionDeclaration
  {
    $$ = new AST_List($2, $1);
  }
	| ClassDeclarations MainFunctionDeclaration ClassDeclarations
	{
    $$ = new AST_List($2, $1);
    $$->append($3);
  }
  ;

//
MainFunctionDeclaration
	: INT MAIN '(' ')' MainFunctionBody
  {
    $$ = $5;
  }
	;

//
MainFunctionBody
	: MainBlock
  {
    $$ = new AST_MainBlock($1);
  }
	;

//
VariableDeclarators
  : VariableDeclarator ',' VariableDeclarators
  {
   $$ = new AST_IDList($1,$3);
  }
  | VariableDeclarator
  {
    $$ = new AST_IDList($1, NULL);
  }
  ;

//create an ID
VariableDeclarator
  : Identifier
  {
    $$ = new AST_ID($1);
  }
  ;

// 
BlockStatement
  : Statement
  {
    $$ = $1;
  }
  ;

//create a mainBlock
MainBlock
	: '{' MainBlockStatements '}'
  {
    $$ = $2;
  }
	| '{' '}'
  {
    $$ = NULL;
  }
	;

//create a list of MainBlockStatement
MainBlockStatements
	: MainBlockStatement MainBlockStatements
  {
    $$ = new AST_StatementList($1, $2);
  }
	| MainBlockStatement
  {
    $$ = new AST_StatementList($1, NULL);
  }
	;

//
MainBlockStatement
	: MainVariableDeclarationStatement
  {
    $$ = $1;
  }
	| BlockStatement
  {
    $$ = $1;
  }
	;

//create a MainVariableDeclarationStatement
MainVariableDeclarationStatement
	: MainVariableDeclaration ';'
  {
    $$ = $1;
  }
	;

//
MainVariableDeclaration
	: Type VariableDeclarators
  {
    $$ = new AST_MainVarDeclStmt($1, $2);
  }
	;

//
OutputStatement
	: OUT Expression ';'
  {
    $$ = new AST_Out($2);
  }
	;

//
ParenExpression
	: '(' Expression ')'
  {
    $$ = $2;
  }
	;

//
ExpressionStatement
	: StatementExpression ';'
  {
    $$ = new AST_ExpressionStmt($1);
  }
	;

//
StatementExpression
	: Assignment
  {
    $$ = $1;
  }
  | MethodInvocation
  {
    $$ = $1;
  }
	;
//
Expression
	: AssignmentExpression
  {
    $$ = $1;
  }
	;

//
AssignmentExpression
	: Assignment
  {
    $$ = $1;
  }
	| EqualityExpression
  {
    $$ = $1;
  }
	;

//
Assignment
	: LeftHandSide AssignmentOperator AssignmentExpression
  {
    $$ = new AST_Assignment($1, $3);
  }
	;

//
LeftHandSide
	: Identifier
  {
    $$ = new AST_Variable($1);
  }
  | FieldAccess
  {
    $$ = $1;
  }
	;

//
EqualityExpression
	: EqualityExpression EQ_OP RelationalExpression
  {
    $$ = new AST_EqualityExpression($1, $3);
  }
	| RelationalExpression
  {
    $$ = $1;
  }
	;

//
RelationalExpression
	: RelationalExpression '<' AdditiveExpression
  {
    $$ = new AST_LessThan($1, $3);
  }
	| RelationalExpression '>' AdditiveExpression
  {
    $$ =  new AST_GreaterThan($1, $3);
  }
	| AdditiveExpression
  {
    $$ = $1;
  }
	;

//
AdditiveExpression
	: AdditiveExpression '+' MultiplicativeExpression
  {
    $$ = new AST_Add($1, $3);
  }
	| AdditiveExpression '-' MultiplicativeExpression
  {
    $$ = new AST_Sub($1, $3);
  }
	| MultiplicativeExpression
  {
    $$ = $1;
  }
	;

MultiplicativeExpression
	: MultiplicativeExpression '*' UnaryExpression
  {
    $$ = new AST_Mul($1, $3);
  }
	| MultiplicativeExpression '/' UnaryExpression
  {
    $$ = new AST_Div($1, $3);
  }
	| UnaryExpression
  {
    $$ = $1;
  }
	;

UnaryExpression
	: '-' UnaryExpression
  {
    $$ = new AST_Negate($2);
  }
	| '!' UnaryExpression
  {
    $$ = new AST_Not($2);
  }
	| CastExpression
  {
    $$ = $1;
  }
	;

CastExpression
  : ParenExpression CastExpression
  {
    $$ = new AST_Cast($1, $2);
  }
	| Primary
  {
    $$ = $1;
  }
  ;

Primary
	: Identifier
  {
    $$ = new AST_Variable($1);
  }
	| PrimaryNoNewArray
  {
    $$ = $1;
  }
	;

PrimaryNoNewArray
	: ParenExpression
  {
    $$ = $1;
  }
  | THIS
  {
    $$ = new AST_This();
  }
  | FieldAccess
  {
    $$ = $1;
  }
  | MethodInvocation
  {
    $$ = $1;
  }
  | ClassInstanceCreationExpression
  {
    $$ = $1;
  }
	| Literal
  {
    $$ = $1;
  }
	;

AssignmentOperator
	: '='
  {
  }
	;

Type
	: ReferenceType
  {
    $$ = $1;
  }
  | PrimitiveType
  {
    $$ = $1;
  }
	;
  
ReferenceType
	: ClassType
  {
    $$ = $1;
  }
	;

ClassType
	: Identifier
  {
    //create a new classType if there is no one
    $$ = types->getOrCreateClassType($1);
  }
	;

PrimitiveType
	: NumericType
  {
    $$ = $1;
  }
	;

NumericType
	: IntegralType
  {
    $$ = $1;
  }
	;

IntegralType
	: INT
  {
    $$ = types->intType();
  }
	;


Identifier
	: IDENTIFIER
  {
    $$ = $1;
  }
	;

Literal
	: INTEGER_LITERAL	
  {
    $$ = new AST_IntegerLiteral($1);
  }
  | NULL_LITERAL
  {
    $$ = new AST_NULL();
  }
	;
  
Block
	: '{' BlockStatements '}'
  {
    $$ = new AST_Block($2);
  }
	| '{' '}'
  {
    $$ = new AST_Block(NULL);
  }
	;

BlockStatements
	: BlockStatement BlockStatements
  {
    $$ = new AST_StatementList($1, $2);
  }
	| BlockStatement
  {
    $$ = new AST_StatementList($1, NULL);
  }
	;

Statement
	: Block
  {
    $$ = $1;
  }
	| EmptyStatement
  {
    $$ = $1;
  }
	| ExpressionStatement
  {
    $$ = $1;
  }
	| IfThenElseStatement
  {
    $$ = $1;
  }
	| WhileStatement
  {
    $$ = $1;
  }
	| ReturnStatement
  {
    $$ = $1;
  }
  | DeleteStatement
  {
    $$ = $1;
  }
	| OutputStatement
  {
    $$ = $1;
  }
	| BreakStatement
  {
    $$ = $1;
  }
	| ContinueStatement
  {
    $$ = $1;
  }
	;

IfThenElseStatement
	: IF ParenExpression Statement ELSE Statement
  {
    $$ = new AST_IfThenElse($2, $3, $5);
  }
	;

WhileStatement
	: WHILE ParenExpression Statement
  {
    $$ = new AST_While($2, $3);
  }
	;

ReturnStatement
	: RETURN ';'
  {
    $$ = new AST_Return(NULL);
  }
	| RETURN Expression ';'
  {
    $$ = new AST_Return($2);
  }
	;

BreakStatement
	: BREAK ';'
  {
    $$ = new AST_Break();
  }
	;

ContinueStatement
	: CONTINUE ';'
  {
    $$ = new AST_Continue();
  }
	;

EmptyStatement
	: ';'
  {
    $$ = new AST_Empty();
  }
	;

ClassDeclarations
	: ClassDeclaration ClassDeclarations
  {
    $$ = new AST_List($1, $2);
  }
	| ClassDeclaration
  {
    $$ = new AST_List($1, NULL);
  }
	;

ClassDeclaration
	: CLASS Identifier ClassBody
  {
    $$ = new AST_ClassDecl($2, $3, NULL);
  }
	| CLASS Identifier EXTENDS ClassType ClassBody
  {
    $$ = new AST_ClassDecl($2, $5, $4);
  }
	;

ClassBody
	: '{' ClassBodyDeclarations '}'
  {
    $$ = $2;
  }
	| '{' '}'
  {
    $$ = NULL;
  }
	;

ClassBodyDeclarations
	: ClassBodyDeclaration ClassBodyDeclarations
  {
    $$ = new AST_List($1, $2);
  }
	| ClassBodyDeclaration
  {
    $$ = new AST_List($1, NULL);
  }
	;

ClassBodyDeclaration
	: ClassMemberDeclaration
  {
    $$ = $1;
  }
  | ConstructorDeclaration
  {
    $$ = $1;
  }
	| DestructorDeclaration
  {
    $$ = $1;
  }
	| ';'
  {
    $$ = NULL;
  }
	;

ClassMemberDeclaration
	: FieldDeclaration
  {
    $$ = $1;
  }
  | MethodDeclaration
  {
    $$ = $1;
  }
	;
  
FieldDeclaration
	: Type VariableDeclarators ';'
  {
    $$ = new AST_FieldDeclStmt($1, $2);
  }
	; 
  
MethodDeclaration
	: Type MethodDeclarator MethodBody
  {
    $$ = new AST_MethodDecl($1, $2, $3);
  }
	;
  
MethodDeclarator
	: Identifier FormalParameters
  {
    $$ = new AST_MethodSign($1, $2);
  }
	;
  
MethodBody
	: Block
  {
    $$ = $1;
  }
	;
  
ConstructorDeclaration
	: ConstructorDeclarator ConstructorBody
  {
    $$ = new AST_ConstrDecl($1, $2);
  }
  ;
  
ConstructorDeclarator
	: Identifier FormalParameters
  {
    $$ = new AST_MethodSign($1, $2);
  }
  ;
  
ConstructorBody
	: '{' ConstructorInvocation BlockStatements '}'
  {
    $$ = new AST_List($2, $3);
  }
	| '{' ConstructorInvocation '}'
  {
    $$ = new AST_List($2, NULL);
  }
	| Block
  {
    $$ = new AST_List(new AST_ConstrInvoc("super", NULL), new AST_List($1, NULL));
  }
	;
  
ConstructorInvocation
	: THIS Arguments ';'
  {
    $$ = new AST_ConstrInvoc("this", $2);
  }
	| SUPER Arguments ';'
  {
    $$ = new AST_ConstrInvoc("super", $2);
  }
	;

  
DestructorDeclaration
	: DestructorDeclarator DestructorBody
  {
    $$ = new AST_DestrDecl($1, $2);
  }
	;
  
DestructorDeclarator
	: '~' Identifier '(' ')'
  {
    $$ = $2;
  }
	;
  
DestructorBody
	: Block
  {
    $$ = new AST_List($1, NULL);
  }
	;  
  
FormalParameters
	: '(' FormalParameterList ')'
  {
    $$ = $2;
  }
	| '(' ')'
  {
    $$ = NULL;
  }
  
FormalParameterList
	: FormalParameter ',' FormalParameterList
  {
    $$ = new AST_List($1, $3);
  }
	| FormalParameter
  {
    $$ = new AST_List($1, NULL);
  }
	;

FormalParameter
	: Type VariableDeclaratorID
  {
    $$ = new AST_Param($1, $2);
  }
	;
  
 VariableDeclaratorID
	: Identifier
  {
    $$ = $1;
  }
  
DeleteStatement
	: DELETE Expression ';'
  {
    $$ = new AST_Delete($2);
  }
	;

  
ClassInstanceCreationExpression
	: NEW ClassType Arguments
  {
    $$ = new AST_CreateObject($2, $3);
  }
	;
 
  
FieldAccess
	: Primary '.' Identifier
  {
    $$ = new AST_FieldAccess($1, $3);
  }
  | SUPER '.' Identifier
  {
    $$ = new AST_FieldAccess(new AST_Super(), $3);
  }
	;
  
MethodInvocation
	: Identifier Arguments
  {
    $$ = new AST_MethodCall(new AST_This(), $1, $2);
  }
	| Primary '.' Identifier Arguments
  {
    $$ = new AST_MethodCall($1, $3, $4);
  }
	| SUPER '.' Identifier Arguments
  {
    $$ = new AST_MethodCall(new AST_Super(), $3, $4);
  }
	;
  
Arguments
  : '(' ArgumentList ')'
  {
    $$ = $2;
  }
	| '(' ')'
  {
    $$ = NULL;
  }
	;
  
ArgumentList
	: Expression ',' ArgumentList 
  {
    $$ = new AST_List($1, $3);
  }
	| Expression
  {
    $$ = new AST_List($1, NULL);
  }
	;



%%

void yyerror(const char *s)
{
  cerr << "line: " << getCurrentSourceLineNumber() << ": parse error - " << s << endl;
}

