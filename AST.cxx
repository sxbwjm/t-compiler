////////////////////////////////////////////////////////////////////////////
//                              AST.cxx
//
// Name  : AST defination for the T language compiler
// Phase : 4
// Date  : May 2013
// Author: Xiaobo Sun
//
// Discription:
//  this file defines dump funtion and constructors of the AST node class 
//
////////////////////////////////////////////////////////////////////////////

#include <iostream>
using namespace std;

#include "AST.h"
#include "Type.h"

// this routine is in scan.ll
int getCurrentSourceLineNumber();

// global type module is in main.cxx
extern TypeModule* types;

/*******************************************************
*                      AST_Node
*******************************************************/
int AST_Node::errCount = 0;

AST_Node::AST_Node()
{
  // by default get the current line number in the scanner
  line = getCurrentSourceLineNumber();
}

AST_Node::~AST_Node()
{
}

void AST_Node::setLineNumber(int lineNumber)
{
  line = lineNumber;
}

int AST_Node::getLineNumber()
{
  return line;
}

void AST_Node::printErr(string errMsg)
{
  cerr << "line " << line <<": " << errMsg << endl;
  errCount++;
}

/*******************************************************
*                  AST_List
*******************************************************/
AST_List::AST_List(AST_Node* newItem, AST_List* list)
{
  item = newItem;
  restOfList = list;
}

AST_List::~AST_List()
{
  delete item;
  if (restOfList != NULL) delete restOfList;
}

void AST_List::dump()
{
  cerr << "[List\n";
  item->dump();
  if (restOfList != NULL)
  {
    restOfList->dump();
  }
  else
  {
    cerr << "[end]\n";
  }
  cerr << "]" << endl;
}

void AST_List::append(AST_List* list)
{
  AST_List* prev = NULL;
  AST_List* next = restOfList;
  
  while(next != NULL)
  {
    prev = next;
    next = next->restOfList;
  }
  
  prev->restOfList = list;
    
}

/*******************************************************
*                AST_Statement
*******************************************************/
AST_Statement::~AST_Statement()
{
}

AST_Statement::AST_Statement()
{
}

/*******************************************************
*               AST_Expression
*******************************************************/
AST_Expression::~AST_Expression()
{
}

AST_Expression::AST_Expression()
{
  type = types->noType();
}


/*******************************************************
*                AST_BinaryOperator
*******************************************************/
AST_BinaryOperator::~AST_BinaryOperator()
{
  delete left;
  delete right;
}

AST_BinaryOperator::AST_BinaryOperator(AST_Expression* l, AST_Expression* r)
{
  left = l;
  right = r;
}

bool AST_BinaryOperator::isCastable(Type* from, Type* to)
{
  //error type
  if(from == types->errorType() || to == types->errorType())
  {
    return false;
  }
  else if(from == to)
  {
    return true;
  }
  //int type
  else if(from == types->intType() || to == types->intType())
  {
    return false;
  }
  //null type
  else if(from == types->nullType() || to == types->nullType())
  {
    return from == types->nullType();
  }
  //class type
  else
  {
    TypeClass* cFrom = (TypeClass*)from;
    TypeClass* cTo = (TypeClass*)to;
    return cFrom->isSubclassOf(cTo) || cTo->isSubclassOf(cFrom);
  }
}

bool AST_BinaryOperator::isAssignable(Type* from, Type* to)
{
  //error type
  if(from == types->errorType() || to == types->errorType())
  {
    return false;
  }
  else if(from == to)
  {
    return true;
  }
  //int type
  else if(from == types->intType() || to == types->intType())
  {
    return false;
  }
  //null type
  else if(from == types->nullType() || to == types->nullType())
  {
    return from == types->nullType();
  }
  //class type
  else
  {
    TypeClass* cFrom = (TypeClass*)from;
    TypeClass* cTo = (TypeClass*)to;
    return cFrom->isSubclassOf(cTo);
  }
}

bool AST_BinaryOperator::CheckTypeConvertion()
{
  //error type
  if ((left->type == types->errorType()) || (right->type == types->errorType()))
  {
    return false;
  }
  
  bool errFlg = false;
  
  switch (convertionType) {
    //numeric check
    case NUM:
      if ((left->type != types->intType()) || (right->type != types->intType()))
      {
        printErr("the operands must be int type!");
        errFlg = true;
      }
      break;
      
    //assignment conversion check
    case ASSIGN:
      //int type
      if(left->type == types->intType())
      {
        if(right->type != types->intType())
        {
          errFlg = true;
        }
      }
      //class type
      else
      {
        
      }
      
    //cast check
    case CAST:
      //int type
      if(left->type == types->intType())
      {
        if(right->type != types->intType())
        {
          errFlg = true;
        }
      }
      //class type
      else
      {
        if(right->type == types->intType())
        {
            errFlg = true;
        }
        else if(right->type == types->nullType())
        {
          
        }
        else
        {
          
        }
      }
      
      break;
      
    default:
      break;
  }
  if(errFlg)
  {
    return false;
  }
  else
  {
    return true;
  }
}
/*******************************************************
*               AST_UnaryOperator
*******************************************************/
AST_UnaryOperator::~AST_UnaryOperator()
{
  delete left;
}

AST_UnaryOperator::AST_UnaryOperator(AST_Expression* l)
{
  left = l;
}

/*******************************************************
*               AST_MainBlock
*******************************************************/
AST_MainBlock::AST_MainBlock(AST_StatementList* list)
{
  stmtList = list;
}

AST_MainBlock::~AST_MainBlock()
{
}

void AST_MainBlock::dump()
{
  cerr << "[Main-block" << endl;
  if(stmtList != NULL)
  {
    stmtList->dump();
  }
  cerr << "]" << endl;
}

/*******************************************************
 *           AST_MainVarDeclStmt
 *******************************************************/
AST_MainVarDeclStmt::AST_MainVarDeclStmt(Type* tp, AST_IDList* list)
{
  type = tp;
  varList = list;
}

AST_MainVarDeclStmt::~AST_MainVarDeclStmt()
{
}

void AST_MainVarDeclStmt::dump()
{
  cerr << "[Main-Variable-Declaration";
  if(varList != NULL)
  {
    cerr << endl;
    varList->dump();
  }
  cerr << "]" << endl;
}


AST_IntegerLiteral::~AST_IntegerLiteral()
{
}

AST_IntegerLiteral::AST_IntegerLiteral(unsigned int in)
{
  type = types->intType();
  value = in;
  isChildOfNegate = false;
}

void AST_IntegerLiteral::dump()
{
  cerr << "[IntegerLiteral-" << value << "-" << type->toString() << "]" << endl;
}

AST_Variable::AST_Variable(char* in)
{
  name = in;
}

string AST_Variable::getName()
{
  string str(name);
  return str;
}

AST_Variable::~AST_Variable()
{
}

void AST_Variable::dump()
{
  cerr << "[Variable-" << name << "-" << type->toString() << "]" << endl;
}

AST_StatementList::AST_StatementList(AST_Statement* s,
  AST_List* l) : AST_List((AST_Node*) s, l)
{
}

AST_StatementList::~AST_StatementList()
{
}

AST_Declaration::AST_Declaration()
{
  type = types->noType();
}

AST_Declaration::~AST_Declaration()
{
}


AST_Assignment::AST_Assignment(AST_Expression* l, AST_Expression* r):
AST_BinaryOperator(l, r)
{
  convertionType = ASSIGN;
}

AST_Assignment::~AST_Assignment()
{
}

void AST_Assignment::dump()
{
  cerr << "[Assignment-" << type->toString() << endl;
  left->dump();
  right->dump();
  cerr << "]" << endl;
}

/*******************************************************
 *                    AST_Out
 *******************************************************/
AST_Out::AST_Out(AST_Expression* v)
{
  exp = (AST_Expression*) v;
}

AST_Out::~AST_Out()
{
}

void AST_Out::dump()
{
  cerr << "[Out" << endl;
  exp->dump();
  cerr << "]" << endl;
}


/*******************************************************
*                    AST_ExpressionStmt
*******************************************************/
AST_ExpressionStmt::AST_ExpressionStmt(AST_Expression* exp)
{
  expStmt = exp;
}

AST_ExpressionStmt::~AST_ExpressionStmt()
{
}

void AST_ExpressionStmt::dump()
{
  cerr << "[Expression-Statement" << endl;
  expStmt->dump();
  cerr << "]" << endl;
}

/*******************************************************
 *                   AST_EqualityExpression
 *******************************************************/
AST_EqualityExpression::AST_EqualityExpression(AST_Expression* l, AST_Expression* r):
AST_BinaryOperator(l, r)
{
  convertionType = CAST;
}

AST_EqualityExpression::~AST_EqualityExpression()
{
}

void AST_EqualityExpression::dump()
{
  cerr << "[Equality-expression-" << type->toString() << endl;
  left->dump();
  right->dump();
  cerr << "]" << endl;
}

/*******************************************************
 *                   AST_LessThan
 *******************************************************/
AST_LessThan::AST_LessThan(AST_Expression* l, AST_Expression* r):
AST_BinaryOperator(l, r)
{
  convertionType = NUM;
}

AST_LessThan::~AST_LessThan()
{
}

void AST_LessThan::dump()
{
  cerr << "[Less-than-" << type->toString() << endl;
  left->dump();
  right->dump();
  cerr << "]" << endl;
}

/*******************************************************
 *                   AST_GreaterThan
 *******************************************************/
AST_GreaterThan::AST_GreaterThan(AST_Expression* l, AST_Expression* r):
AST_BinaryOperator(l, r)
{
  convertionType = NUM;
}

AST_GreaterThan::~AST_GreaterThan()
{
}

void AST_GreaterThan::dump()
{
  cerr << "[Greater-than-" << type->toString() << endl;
  left->dump();
  right->dump();
  cerr << "]" << endl;
}

/*******************************************************
 *                   AST_Add
 *******************************************************/
AST_Add::AST_Add(AST_Expression* l, AST_Expression* r) :
AST_BinaryOperator(l, r)
{
  convertionType = NUM;
}

AST_Add::~AST_Add()
{
}

void AST_Add::dump()
{
  cerr << "[Add-" << type->toString() << endl;
  left->dump();
  right->dump();
  cerr << "]" << endl;
}

/*******************************************************
 *                   AST_Sub
 *******************************************************/
AST_Sub::AST_Sub(AST_Expression* l, AST_Expression* r) :
AST_BinaryOperator(l, r)
{
  convertionType = NUM;
}

AST_Sub::~AST_Sub()
{
}

void AST_Sub::dump()
{
  cerr << "[Subtract-" << type->toString() << endl;
  left->dump();
  right->dump();
  cerr << "]" << endl;
}

/*******************************************************
 *                   AST_Mul
 *******************************************************/
AST_Mul::AST_Mul(AST_Expression* l, AST_Expression* r) :
AST_BinaryOperator(l, r)
{
  convertionType = NUM;
}

AST_Mul::~AST_Mul()
{
}

void AST_Mul::dump()
{
  cerr << "[Mutiply-" << type->toString() << endl;
  left->dump();
  right->dump();
  cerr << "]" << endl;
}

/*******************************************************
 *                   AST_Div
 *******************************************************/
AST_Div::AST_Div(AST_Expression* l, AST_Expression* r) :
  AST_BinaryOperator(l, r)
{
  convertionType = NUM;
}

AST_Div::~AST_Div()
{
}

void AST_Div::dump()
{
  cerr << "[Divide-" << type->toString() << endl;
  left->dump();
  right->dump();
  cerr << "]" << endl;
}


/*******************************************************
 *                   AST_Deref
 *******************************************************/
AST_Deref::AST_Deref(AST_Expression* l) : AST_UnaryOperator(l)
{
}

AST_Deref::~AST_Deref()
{
}

void AST_Deref::dump()
{
  cerr << "[Deref-" << type->toString() << endl;
  left->dump();
  cerr << "]" << endl;
}

/*******************************************************
 *                   AST_Negate
 *******************************************************/
AST_Negate::AST_Negate(AST_Expression* l): AST_UnaryOperator(l)
{
  //set the isChildOfNegate flag if its child node is integer literal
  AST_IntegerLiteral* intLiteral = dynamic_cast<AST_IntegerLiteral*>(l);
  if(intLiteral != NULL)
  {
    intLiteral->isChildOfNegate = true;
  }
}

AST_Negate::~AST_Negate()
{
}

void AST_Negate::dump()
{
  cerr << "[Negate-" << type->toString() << endl;
  left->dump();
  cerr << "]" << endl;
}

/*******************************************************
 *                   AST_Not
 *******************************************************/
AST_Not::AST_Not(AST_Expression* l): AST_UnaryOperator(l)
{
}

AST_Not::~AST_Not()
{
}

void AST_Not::dump()
{
  cerr << "[Not-" << type->toString() << endl;
  left->dump();
  cerr << "]" << endl;
}

/*******************************************************
 *                   AST_ID
 *******************************************************/
AST_ID::AST_ID(char* s)
{
  name = s;
}

AST_ID::~AST_ID()
{
}

void AST_ID::dump()
{
  cerr << "[Indentifer-" << name << "-" << type->toString() << "]" << endl;
}
/*******************************************************
 *                   AST_IDList
 *******************************************************/
AST_IDList::AST_IDList(AST_ID* id, AST_List* list):AST_List( (AST_Node*)id, list)
{
}

AST_IDList::~AST_IDList()
{
}

/*******************************************************
 *                    AST_Block
 *******************************************************/
AST_Block::AST_Block(AST_StatementList* list)
{
  stmtList = list;
}

AST_Block::~AST_Block()
{
  if(stmtList != NULL)
  {
    delete stmtList;
  }
}

void AST_Block::dump()
{
  cerr << "[block";
  if(stmtList != NULL)
  {
    cerr << endl;
    stmtList->dump();
  }
  cerr << "]" << endl;
}

/*******************************************************
 *                    AST_IfThenElse
 *******************************************************/
int AST_IfThenElse::labelNum = 0;

AST_IfThenElse::  AST_IfThenElse(AST_Expression* exp,
                                 AST_Statement* stmt1,
                                 AST_Statement* stmt2)
{
  condition = exp;
  ifStmt = stmt1;
  elseStmt = stmt2;
}

AST_IfThenElse::~AST_IfThenElse()
{
  delete condition;
  delete ifStmt;
  delete elseStmt;
}

void AST_IfThenElse::dump()
{
  cerr << "[IfThenElse" << endl;
  condition->dump();
  ifStmt->dump();
  elseStmt->dump();
  cerr << "]" << endl;
}

/*******************************************************
 *                    AST_While
 *******************************************************/
int AST_While::count = 0;
stack<unsigned int> AST_While::whileStack;

AST_While::AST_While(AST_Expression* exp, AST_Statement* stmt)
{
  condition = exp;
  whileStmt = stmt;
}

AST_While::~AST_While()
{
  delete condition;
  delete whileStmt;
}

void AST_While::dump()
{
  cerr << "[while" << endl;
  condition->dump();
  whileStmt->dump();
  cerr << "]" << endl;
}

/*******************************************************
 *                    AST_Return
 *******************************************************/
AST_Return::AST_Return(AST_Expression* exp)
{
  returnExp = exp;
}

AST_Return::~AST_Return()
{
  if(returnExp != NULL)
  {
    delete returnExp;
  }
}

void AST_Return::dump()
{
  cerr << "[return";
  if(returnExp != NULL)
  {
    cerr << endl;
    returnExp->dump();
  }
  cerr << "]" << endl;
}

/*******************************************************
 *                    AST_Break
 *******************************************************/
AST_Break::AST_Break()
{
}

AST_Break::~AST_Break()
{
}

void AST_Break::dump()
{
  cerr << "[break]" << endl;
}

/*******************************************************
 *                    AST_Continue
 *******************************************************/
AST_Continue::AST_Continue()
{
}

AST_Continue::~AST_Continue()
{
}

void AST_Continue::dump()
{
  cerr << "[continue]" << endl;
}

/*******************************************************
 *                    AST_Empty
 *******************************************************/
AST_Empty::AST_Empty()
{
}

AST_Empty::~AST_Empty()
{
}

void AST_Empty::dump()
{
  cerr << "[empty-statment]" << endl;
}

/*******************************************************
 *                    AST_ClassDecl
 *******************************************************/
AST_ClassDecl::AST_ClassDecl(string name,AST_List* body, Type* superClass)
{
  className = name;
  classBody = body;
  if(superClass == NULL)
  {
    superClassType = types->getOrCreateClassType("Object");
  }
  else
  {
  superClassType = (TypeClass*)superClass;
  }
}

AST_ClassDecl::~AST_ClassDecl()
{
}

void AST_ClassDecl::dump()
{
  cerr << "[Class-declaration-" << className
       << "(super:" 
       << (superClassType == NULL? "NULL": superClassType->toString() )
       << ")";
        
  if(classBody != NULL)
  {
    cerr << endl;
    classBody->dump();
  }
  cerr << "]" << endl;  
}

/*******************************************************
 *                    AST_Cast
 *******************************************************/
AST_Cast::AST_Cast(AST_Expression* l, AST_Expression* r):
AST_BinaryOperator(l, r)
{
  convertionType = CAST;
}

AST_Cast::~AST_Cast()
{
}

void AST_Cast::dump()
{
  cerr << "[cast-" << type->toString() << endl;
 
  if(left != NULL)
  {
    left->dump();
  }
  if(right != NULL)
  {
    right->dump();
  }
  cerr << "]" << endl;
}

/*******************************************************
 *                    AST_NULL
 *******************************************************/
AST_NULL::AST_NULL()
{
}

AST_NULL::~AST_NULL()
{
}

void AST_NULL::dump()
{
  cerr << "[NULL-Literal-" << type->toString() << "]" << endl;
}


/*******************************************************
 *                    AST_FieldDeclStmt
 *******************************************************/
AST_FieldDeclStmt::AST_FieldDeclStmt(Type* tp, AST_List* list)
{
  type = tp;
  fieldList = (AST_IDList*)list;
}

AST_FieldDeclStmt::~AST_FieldDeclStmt()
{
}

void AST_FieldDeclStmt::dump()
{
  cerr << "[Field-Declaration";
  if(fieldList != NULL)
  {
    cerr << endl;
    fieldList->dump();
  }
  cerr << "]" << endl;
}

/*******************************************************
 *                    AST_CreateObject
 *******************************************************/
int AST_CreateObject::labelNum = 0;

AST_CreateObject::AST_CreateObject(Type* tp, AST_List* list)
{
  type = tp;
  argList = list;
}

AST_CreateObject::~AST_CreateObject()
{
}

void AST_CreateObject::dump()
{
  cerr << "[Create-Instance " << type->toString();
  if(argList != NULL)
  {
    cerr << endl;
    argList->dump();
  }
  cerr << "]" << endl;
}

/*******************************************************
 *                    AST_FieldAccess
 *******************************************************/
AST_FieldAccess::AST_FieldAccess(AST_Expression* exp, string field):
AST_UnaryOperator(exp)
{
  //classExp = exp;
  fieldName = field;
}

AST_FieldAccess::~AST_FieldAccess()
{
}

void AST_FieldAccess::dump()
{
  cerr << "[Field-access-" << type->toString() << "-field-name(" << fieldName << ")"
       << "-offset(" << offset << ")";
  if(left != NULL)
  {
    cerr << endl;
    left->dump();
  }
  cerr << "]" << endl;
}

/*******************************************************
 *                    AST_This
 *******************************************************/
AST_This::AST_This()
{
 
}

AST_This::~AST_This()
{
}

void AST_This::dump()
{
  cerr << "[This-" << type->toString() << "]" << endl;

}



/*******************************************************
 *                    AST_MethodDecl
 *******************************************************/
AST_MethodDecl::AST_MethodDecl(Type* tp, AST_Declaration* name, AST_Statement* body)
{
  type = tp;
  methodSign = name;
  methodBody = body;
}

AST_MethodDecl::~AST_MethodDecl()
{
}

void AST_MethodDecl::dump()
{
  cerr << "[Method-Declaration-" << type->toString() << endl;
  if(methodSign != NULL)
  {
    methodSign->dump();
  }
  if(methodBody != NULL)
  {
    methodBody->dump();
  }
  cerr << "]" << endl;
}


/*******************************************************
 *                    AST_MethodSign
 *******************************************************/
AST_MethodSign::AST_MethodSign(string name, AST_List* params)
{
  methodName = name;
  paramList = params;
}

AST_MethodSign::~AST_MethodSign()
{
}

void AST_MethodSign::dump()
{
  cerr << "[MethodSignature(" << methodName << ")" << type->toString();
  if(paramList != NULL)
  {
    cerr << endl;
    paramList->dump();
  }
  cerr << "]" << endl;
}

/*******************************************************
 *                    AST_ConstrDecl
 *******************************************************/
AST_ConstrDecl::AST_ConstrDecl(AST_Declaration* name, AST_List* body)
{
  constrSign = (AST_MethodSign*)name;
  constrBody = body;
}

AST_ConstrDecl::~AST_ConstrDecl()
{
}

void AST_ConstrDecl::dump()
{
  cerr << "[Constructor-declaration" << type->toString() << endl;
  if(constrSign != NULL)
  {
    constrSign->dump();
  }
  if(constrBody != NULL)
  {
    constrBody->dump();
  }
  
  cerr << "]" << endl;

}

/*******************************************************
 *                    AST_ConstrInvoc
 *******************************************************/
AST_ConstrInvoc::AST_ConstrInvoc(string which, AST_List* args):
AST_MethodCall(NULL, NULL, args)
{
  thisOrSuper = which;
  argList = args;
  method = NULL;
}

AST_ConstrInvoc::~AST_ConstrInvoc()
{
}

void AST_ConstrInvoc::dump()
{
  cerr << "[Constructor-Invoke(" << thisOrSuper << ")";
  if(leftExp != NULL)
  {
    cerr << endl;
    leftExp->dump();
  }
  if(argList != NULL)
  {
    //cerr << endl;
    argList->dump();
  }
  cerr << "]" << endl;
}

/*******************************************************
 *                    AST_DestrDecl
 *******************************************************/
AST_DestrDecl::AST_DestrDecl(string name, AST_List* body)
{
  methodName = name; // string(name);
  methodBody = body;
}

AST_DestrDecl::~AST_DestrDecl()
{
}

void AST_DestrDecl::dump()
{
  cerr << "[Destructor-Declaration" << endl;
  if(methodBody != NULL)
  {
    cerr << endl;
    methodBody->dump();
  }
  cerr << "]";
}

/*******************************************************
 *                    AST_Param
 *******************************************************/
AST_Param::AST_Param(Type* tp, string parId)
{
  type = tp;
  paramName = parId;
}

AST_Param::~AST_Param()
{
}

void AST_Param::dump()
{
  cerr << "[Parameter-Declaration(" << paramName << ")" << type->toString() << "]";
}

/*******************************************************
 *                    AST_Param
 *******************************************************/
AST_Delete::AST_Delete(AST_Expression* exp)
{
  deleteExp = exp;
}

AST_Delete::~AST_Delete()
{
}

void AST_Delete::dump()
{
  cerr << "[Delete";
  if(deleteExp != NULL)
  {
    cerr << endl;
    deleteExp->dump();
  }
  cerr << "]";
}
/*******************************************************
 *                    AST_Super
 *******************************************************/
AST_Super::AST_Super()
{
  
}

AST_Super::~AST_Super()
{
}

void AST_Super::dump()
{
  cerr << "[Super]" << endl;
}

/*******************************************************
 *                    AST_MethodCall
 *******************************************************/
AST_MethodCall::AST_MethodCall(AST_Expression* exp, char* name, AST_List* args)
{
  leftExp = exp;
  if(name != NULL)
  {
    methodName = string(name);
  }
  else
  {
    methodName = "";
  }
  argList = args;
  isConstr = false;
  mungedName = "";
}

AST_MethodCall::~AST_MethodCall()
{
}

void AST_MethodCall::dump()
{
  cerr << "[Method-Call(" << methodName << ")-" << type->toString();
  if(leftExp != NULL)
  {
    cerr << endl;
    leftExp->dump();
  }
  if (argList != NULL)
  {
    argList->dump();
  }
  cerr << "]";
}









  
  


