#ifndef _TYPE_H
#define _TYPE_H


// May 2013
// Type representation for T language
//
// NOTE: don't construct Type objects directly! Use a TypeModule.
//
// A TypeModule is simply a mechanism for managing the memory used
// to represent types. The strategy is to only allocate memory for
// a distinct type once and re-use that memory for subsequent calls
// to construct that type. A TypeModule keeps track of all the
// allocated types so that memory can be reclaimed when the types
// are no longer needed.
//
// So, to obtain a type, access the appropriate member function for
// the global type module object, which is created in main.cxx.

using namespace std;
#include <vector>
#include <map>
#include <string>
#include <iostream>

//declared in AST.h
class AST_Declaration;

// abstract base class
class Type
{
  public:
    string name;
    virtual ~Type();
    virtual char* toString() = 0;
    bool isCastableTo(Type* to);
    bool isAssignableTo(Type* to);

  protected:
    Type();
};

class TypeNone: public Type
{
  public:
    TypeNone();
    ~TypeNone();

    char* toString();
};

class TypeError: public Type
{
  public:
    TypeError();
    ~TypeError();

    char* toString();
};

class TypeInt: public Type
{
  public:
    TypeInt();
    ~TypeInt();

    char* toString();
  
    //char* name;

};

class TypeNull: public Type
{
public:
  TypeNull();
  ~TypeNull();
  
  char* toString();
};

class Method
{
public:
  string className;
  string name;
  string mungedName;
  Type* type;
  int offset;
  vector<string> argNameList;
  vector<Type*> argTypeList;
  //pointed back to AST node;
  AST_Declaration* astConstrDecl;
public:
  Method(string cName);
  void addArg(string name, Type* type);
  bool isParamExist(string name);
  string toString();
  string createMungedName();
  
};

class TypeClass: public Type
{
  public:
    TypeClass(string cName);
    ~TypeClass();
  
    int getfieldsNumIncludeSuper(TypeClass* classType);
    void updateFieldsOffset();
    int getFieldOffset(string name);
    Type* getFieldType(string name);
    bool isMethodExist(Method* m);
    bool isConstrExist(Method* m);
    bool isSubclassOf(TypeClass* cType);
    void createVMT();
    string getMungedNameWithoutClass(string name);
    //int getMostSpecMethodOffset(string name,vector<Type*> args, string mType);
    Method* getMostSpecMethod(string mType, string name, vector<Type*> args);
    //int getOffsetOfMethod(string name);
    char* toString();
    string className;
    map<string, Type*> fieldsList;
    map<string, int> fieldsOffset;
    vector<Method*> methodsList;
    vector<Method*> constrList;
    Method* destructor;
  
     
    //vmt
    vector<Method*> vmt;

    TypeClass* parenClass;
    bool isDeclared;
    bool isError;
    bool isVMTCreated;
protected:
  void createVMT(TypeClass* cType);
 
};

class TypeModule
{
  protected:
     Type* intTypeInternal;
     Type* nullTypeInternal;
     Type* errorTypeInternal;
     Type* noTypeInternal;
     map<string, TypeClass*> classTypeList;
    
  public:
    TypeModule();
    ~TypeModule();
    void Initialize();
    Type* intType();
    Type* nullType();
    TypeClass* getOrCreateClassType(string name);
    Type* errorType();
    Type* noType();
  
    void updateInfo();
    void dumpClasses();
};

#endif
