////////////////////////////////////////////////////////////////////////////
//                              encode.cxx
//
// Name  : AST code generation for the T language compiler
// Phase : 4
// Date  : May 2013
// Author: Xiaobo Sun
//
// Discription:
//  this file defines the AST methods for code generation
//
////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>

#include <iostream>
using namespace std;

#include "AST.h"
#include "Type.h"

// global type module is in main.cxx
extern TypeModule* types;
int labelNum = 0;


// output the prelude code
void encodeInitialize()
{
  //VMT for class "Object"
  cout << "#\tVMT[class Object]\n";
  cout << "\t.data\n";
  cout << "\tObject$VMT:\n";
  cout << "\t.long 0\n";
  cout << "\t.long Object$$$Object\n";
  cout << "\t.long Object$equals$Object\n";
  cout << "\t.text\n";
  
  
  //constructor Object
  cout << "\n#\tObject()\n";
  cout << "\t.align  4\n";
  cout << "\t.globl  Object$$Object\n";
  cout << "Object$$Object:\n";
  cout << "\tpushl   %ebp\n";
  cout << "\tmovl    %esp, %ebp\n";
 // cout << "\tmovl    $0, %eax\n";
  cout << "Object$Object$exit:\n";
  cout << "\tpopl    %ebp\n";
  cout << "\tret\n";
  
  //equals method of Object
  cout << "\n#\tequals()\n";
  cout << "\t.align  4\n";
  cout << "\t.globl  Object$equals$Object\n";
  cout << "Object$equals$Object:\n";
  cout << "\tpushl   %ebp\n";
  cout << "\tmovl    %esp, %ebp\n";
  
  
  cout << "\tmovl\t8(%ebp),\t%eax\n";
  cout << "\tmovl\t12(%ebp),\t%edx\n";
  cout << "\tcmpl\t%eax,%edx\n";
  cout << "\tsete\t%al\n";
  cout << "\tmovzbl\t%al, %eax\n";
  
  cout << "Object$equals$Object$exit:\n";
  cout << "\tpopl    %ebp\n";
  cout << "\tret\n";
  
  //destructor method of Object
  cout << "\n#\t~Object(Object)\n";
  cout << "\t.align  4\n";
  cout << "\t.globl  Object$$$Object\n";
  cout << "Object$$$Object:\n";
  cout << "\tpushl   %ebp\n";
  cout << "\tmovl    %esp, %ebp\n";
  cout << "\tmovl    $0, %eax\n";
  cout << "Object$$Object$exit:\n";
  cout << "\tpopl    %ebp\n";
  cout << "\tret\n";

  

}

// output the epilogue code
void encodeFinish()
{
}

void AST_List::encode()
{
  item->encode();
  if (restOfList != NULL) restOfList->encode();
}

void AST_IntegerLiteral::encode()
{
  cout << "\n#\tIntegerLiteral\n";
  cout << "\tpushl\t$" << value << endl;
}

void AST_Variable::encode()
{
  
  if(varScope == "main")
  {
    cout << "\n#\tMain Variable\n";
    cout << "\tpushl\t$mainvar$" << name << endl;
  }
  else if(varScope == "param")
  {
    cout << "\n#\tMethod Parameter\n";
    cout << "\tleal  " << (varPosOrOffset * 4)+12 << "(%ebp), %eax\n";
    cout << "\tpushl %eax\n";
  }
  else if(varScope == "field")
  {
    cout << "\n#\tClass Field\n";
    cout << "\tmovl  8(%ebp), %eax\n";
    cout << "\taddl  $" << varPosOrOffset << ", %eax\n";
    cout << "\tpushl %eax\n";
  }
}

void AST_MainVarDeclStmt::encode()
{
  varList->encode();
}

void AST_MainBlock::encode()
{
  AST_While::count = 0;
  
  cout << "\n#\tMain Block Prologue\n";
  cout << "\t.text\n";
  cout << "\t.align 4\n";
  cout << "\t.globl main\n";
  cout << "\tmain:\n";
  cout << "\tpushl %ebp\n";
  cout << "\tmovl %esp, %ebp\n";
  
  if(stmtList != NULL)
  {
    stmtList->encode();
  }
  
  cout << "\n#\tMain Block Epilogue\n";
  cout << "main$exit:\n";
  cout << "\tpopl\t%ebp\n";
  cout << "\tret\n";
   
}

void AST_Assignment::encode()
{
  left->encode();
  right->encode();

  cout << "\n#\tAssignment\n";
  cout << "\tpopl\t%eax\n";
  cout << "\tpopl\t%edx\n";
  cout << "\tmovl\t%eax, (%edx)\n";
  cout << "\tpushl %eax\n";
}

void AST_Out::encode()
{
  exp->encode();

  if (exp->type == types->intType())
  {
    cout << "\n#\tPrint int\n";
    cout << "\tcall\tRTS_outputInteger\n";
    cout << "\taddl\t$4, %esp\n";
  }
  else if (exp->type == types->errorType())
  {
    // do nothing: there was a semantic error
  }
  else
  {
    cerr << line << ": BUG in AST_Out::encode: unknown type\n"; 
    exit(-1);
  }
}

void AST_ExpressionStmt::encode()
{
  expStmt->encode();
  
  cout << "\n#Expression Statment\n";
  cout << "\taddl\t$4, %esp\n";
}

void AST_EqualityExpression::encode()
{
  left->encode();
  right->encode();
  
  cout << "\n#\tEquality (==)\n";
  
  cout << "\tpopl\t%eax\n";
  cout << "\tpopl\t%edx\n";
  cout << "\tcmpl\t%eax,%edx\n";
  cout << "\tsete\t%al\n";
  cout << "\tmovzbl\t%al, %eax\n";
  cout << "\tpushl\t%eax\n";
}


void AST_LessThan::encode()
{
  left->encode();
  right->encode();
  
  cout << "\n#\tLess than (<)\n";
  
  cout << "\tpopl\t%eax\n";
  cout << "\tpopl\t%edx\n";
  cout << "\tcmpl\t%eax,%edx\n";
  cout << "\tsetl\t%al\n";
  cout << "\tmovzbl\t%al, %eax\n";
  cout << "\tpushl\t%eax\n";
}

void AST_GreaterThan::encode()
{
  left->encode();
  right->encode();
  
  cout << "\n#\tGreater than (>)\n";
  
  cout << "\tpopl\t%eax\n";
  cout << "\tpopl\t%edx\n";
  cout << "\tcmpl\t%eax,%edx\n";
  cout << "\tsetg\t%al\n";
  cout << "\tmovzbl\t%al, %eax\n";
  cout << "\tpushl\t%eax\n";
}

void AST_Add::encode()
{  
  left->encode();
  right->encode();

  cout << "\n#\tAdd (+)\n";
  
  cout << "\tpopl  %edx\n";
  cout << "\tpopl  %eax\n";
  cout << "\taddl  %edx, %eax\n";
  cout << "\tpushl %eax\n";
}

void AST_Sub::encode()
{
  left->encode();
  right->encode();

  cout << "\n#\tSubtract (-)\n";
  
  cout << "\tpopl  %edx\n";
  cout << "\tpopl  %eax\n";
  cout << "\tsubl  %edx, %eax\n";
  cout << "\tpushl %eax\n";
}

void AST_Mul::encode()
{
  left->encode();
  right->encode();

  cout << "\n#\tMultiply (*)\n";
  
  cout << "\tpopl\t%eax\n";
  cout << "\tpopl\t%edx\n";
  cout << "\timull\t%edx, %eax\n";
  cout << "\tpushl\t%eax\n";
}

void AST_Div::encode()
{
  left->encode();
  right->encode();

  if (type == types->intType())
  {
    cout << "\n#\tDivide int\n";
    
    cout << "\tpopl\t%ecx\n";
    cout << "\tcmpl\t$0, %ecx\n";
    cout << "\tje\tLABEL_DIV_ERR" << labelNum <<"\n";
    cout << "\tpopl\t%eax\n";
    cout << "\tcltd\n";
    cout << "\tidivl\t%ecx, %eax\n";
    cout << "\tpushl\t%eax\n";
    cout << "\tjmp\tLABEL_DIV" << labelNum <<"\n";
    
    cout << "LABEL_DIV_ERR" << labelNum <<":\n";
    cout << "\tpushl\t$" << line <<"\n";
    cout << "\tcall\tRTS_printDivideByZeroError\n";
    
    cout << "LABEL_DIV" << labelNum <<":\n";
    
    labelNum++;
  }
  else if (type == types->errorType())
  {
    // do nothing: there was a semantic error
  }
  else
  {
    cerr << line << ": BUG in AST_Divide::encode: unexpected type\n"; 
    exit(-1);
  }
}

void AST_Deref::encode()
{
  left->encode();

  // both types are 32 bits so no need to distinguish types here?
  cout << "\n#\tDeref\n";
  cout << "\tpopl\t%eax\n";
  cout << "\tmovl\t(%eax), %eax\n";
  cout << "\tpushl\t%eax\n";
}

void AST_Negate::encode()
{
  left->encode();
  
  cout << "\n#\tNegate (unary -)\n";
  
  cout << "\tpopl\t%eax\n";
  cout << "\tnegl\t%eax\n";
  cout << "\tpushl\t%eax\n";
}

void AST_Not::encode()
{
  left->encode();
  
  cout << "\n#\tLogical Complement (!)\n";
  
  cout << "\tpopl\t%eax\n";
  cout << "\tcmpl\t$0,%eax\n";
  cout << "\tsete\t%al\n";
  cout << "\tmovzbl\t%al, %eax\n";
  cout << "\tpushl\t%eax\n";
}


void AST_ID::encode()
{
  cout << "\n#\tMain Variable Declaration\n";
  cout << "\t.data\n";
  cout << "\tmainvar$"<< name << ": .long 0\n";
  cout << "\t.text\n";
}

/*******************************************************
 *            AST_Block
 *******************************************************/
void AST_Block::encode()
{
  if(stmtList != NULL)
  {
    stmtList->encode();
  }
}

/*******************************************************
 *            AST_IfThenElse
 *******************************************************/
void AST_IfThenElse::encode()
{
  int num = labelNum++;
  
  cout << "\n#\tif then else\n";
  
  //condition
  condition->encode();
  
  cout << "\tpopl %eax\n";
  cout << "\tcmpl $0, %eax\n";
  cout << "\tje IF_ELSE_" << num <<"\n";
  
  //if statement
  ifStmt->encode();
  
  cout << "\tjmp IF_END_" << num << "\n";
  cout << "IF_ELSE_" << num << ":\n";
  //else statment
  elseStmt->encode();
  
  cout << "IF_END_" << num << ":\n";
  
}
/*******************************************************
 *            AST_While
 *******************************************************/
void AST_While::encode()
{
  int labelNum = ++count;
  whileStack.push(count);
  
  cout << "\n#\twhile\n";
  cout << "While_Start_" << labelNum << ":\n";
  
  //condition
  condition->encode();
  
  cout << "\tpopl %eax\n";
  cout << "\tcmpl $0, %eax\n";
  cout << "\tje While_End_" << labelNum << "\n";
  
  //while statement
  whileStmt->encode();
  cout << "\tjmp While_Start_" << labelNum << "\n";
  cout << "While_End_" << labelNum << ":\n";
  
  whileStack.pop();
  
}



/*******************************************************
 *            AST_Return
 *******************************************************/
void AST_Return::encode()
{
  cout << "\n#\treturn\n";
  
  if(returnExp != NULL)
  {
    returnExp->encode();
    cout << "\tpopl %eax\n";
  }
  
  if(curMethod == NULL)
  {
    cout << "\tjmp main$exit\n";
  }
  else
  {
    cout << "\tjmp " << curMethod->mungedName << "$exit\n";
  }
}

/*******************************************************
 *            AST_Break
 *******************************************************/
void AST_Break::encode()
{
  cout << "\n#\tbreak\n";
  
  cout << "\tjmp While_End_" << AST_While::whileStack.top() << "\n";
  

}

/*******************************************************
 *            AST_Continue
 *******************************************************/
void AST_Continue::encode()
{
  cout << "\n#\tcontinue\n";
  
  cout << "\tjmp While_Start_" << AST_While::whileStack.top() << "\n";
  

}
/*******************************************************
 *            AST_Empty
 *******************************************************/
void AST_Empty::encode()
{
}

/*******************************************************
 *                    AST_ClassDecl
 *******************************************************/
void AST_ClassDecl::encode()
{
  //VMT for class
  cout << "\n#\tVMT[class " << className << "]\n";
  cout << "\t.data\n";
  cout << "\t" << className << "$VMT:\n";
  cout << "\t.long " << superClassType->className << "$VMT\n";
  
  for(unsigned int i=1; i<classType->vmt.size(); i++)
  {
    cout << "\t.long " << classType->vmt[i]->mungedName << "\n";
  }
  
  cout << "\t.text\n";
  
  classBody->encode();

}

/*******************************************************
 *                    AST_Cast
 *******************************************************/
void AST_Cast::encode()
{
  right->encode();
  
  TypeClass* cType = (TypeClass*)type;
  cout << "\n#\tCAST\n";
  cout << "\tpushl $" << cType->className << "$VMT\n";
  cout << "\tpushl $" << line << "\n";
  cout << "\tcall  RTS_checkCast\n";
  cout << "\taddl  $8, %esp\n";
  
}

/*******************************************************
 *                    AST_NULL
 *******************************************************/
void AST_NULL::encode()
{
  cout << "\n#\tNULL\n";
  
  cout << "\tpushl $0\n";
}


/*******************************************************
 *                    AST_FieldDeclStmt
 *******************************************************/
void AST_FieldDeclStmt::encode()
{
}

/*******************************************************
 *                    AST_CreateObject
 *******************************************************/
void AST_CreateObject::encode()
{
  TypeClass* classType = (TypeClass*)type;
  int fieldsNum = classType->getfieldsNumIncludeSuper(classType);
  
  //encode arguments
  AST_List* list = argList;
  int n = 0;
  while(list != NULL)
  {
    AST_Expression* exp = (AST_Expression*)list->item;
    exp->encode();
    list = list->restOfList;
    n++;
  }
  
  cout << "\n#\tcreate instance\n";
  cout << "\tpushl $" << n << "\n";
  cout << "\tcall  RTS_reverseArgumentsOnStack\n";
  cout << "\tpopl  %ecx\n";
  cout << "\tpushl $4\n";
  cout << "\tpushl $" << fieldsNum << "\n";
  cout << "\tcall  calloc\n";
  cout << "\taddl  $8, %esp\n";
  cout << "\tcmpl  $0, %eax\n";
  cout << "\tjne   " << classType->className << "$L" << labelNum << "\n";
  cout << "\tpushl $" << line << "\n";
  cout << "\tcall  RTS_outOfMemoryError\n";
  cout <<  classType->className <<"$L" << labelNum << ":\n";
  cout << "\tmovl  $" << classType->className << "$VMT, (%eax)\n";
  cout << "\tpushl %eax\n";
  cout << "\tcall  " << constrMungedName << "\n";
  cout << "\tpopl  %eax\n";
  cout << "\taddl  $" << n * 4 << ", %esp\n";
  cout << "\tpushl %eax\n";
  
  
  labelNum++;

}

/*******************************************************
 *                    AST_FieldAccess
 *******************************************************/
void AST_FieldAccess::encode()
{
  left->encode();
  cout << "\n#\tfield reference\n";
  cout << "\tpushl $" << line << "\n";
  cout << "\tcall  RTS_checkForNullReference\n";
  cout << "\tpopl  %eax\n";
  cout << "\tpopl  %eax\n";
  cout << "\taddl  $" << offset << ", %eax\n";
  cout << "\tpushl %eax\n";
}

/*******************************************************
 *                    AST_This
 *******************************************************/
void AST_This::encode()
{
  cout << "\n#\tthis\n";
  cout << "\tmovl  8(%ebp), %eax\n";
  cout << "\tpushl %eax\n";

}

/*******************************************************
 *                    AST_MethodDecl
 *******************************************************/
void AST_MethodDecl::encode()
{
  AST_MethodSign* mSign = (AST_MethodSign*)methodSign;
  string mungedName = mSign->method->mungedName;
  cout << "\n#\tmethod body\n";
  cout << "\t.align  4\n";
  cout << "\t.globl  " << mungedName << "\n";
  cout << mungedName << ":\n";
  cout << "\tpushl   %ebp\n";
  cout << "\tmovl    %esp, %ebp\n";
  
  methodBody->encode();
  
  cout << "\tmovl    $0, %eax\n";
  cout << mungedName << "$exit:\n";
  cout << "\tpopl    %ebp\n";
  cout << "\tret\n";
}

/*******************************************************
 *                    AST_MethodSign
 *******************************************************/
void AST_MethodSign::encode()
{
  
}

/*******************************************************
 *                    AST_ConstrDecl
 *******************************************************/
void AST_ConstrDecl::encode()
{
  AST_MethodSign* mSign = (AST_MethodSign*)constrSign;
  string mungedName = mSign->method->mungedName;
  cout << "\n#\tConstructor body\n";
  cout << "\t.align  4\n";
  cout << "\t.globl  " << mungedName << "\n";
  cout << mungedName << ":\n";
  cout << "\tpushl   %ebp\n";
  cout << "\tmovl    %esp, %ebp\n";
  
  constrBody->encode();
  
  cout << "\tmovl    $0, %eax\n";
  cout << mungedName << "$exit:\n";
  cout << "\tpopl    %ebp\n";
  cout << "\tret\n";
}


/*******************************************************
 *                    AST_DestrDecl
 *******************************************************/
void AST_DestrDecl::encode()
{
  //AST_MethodSign* mSign = (AST_MethodSign*)methodSign;
  string mungedName = methodName + "$$$" + methodName;
  cout << "\n#\tmethod body\n";
  cout << "\t.align  4\n";
  cout << "\t.globl  " << mungedName << "\n";
  cout << mungedName << ":\n";
  cout << "\tpushl   %ebp\n";
  cout << "\tmovl    %esp, %ebp\n";
  
  methodBody->encode();
  
  cout << "\tmovl    $0, %eax\n";
  cout << mungedName << "$exit:\n";
  
  //call destructor of parent class
  cout << "\tcall    " << superDestrMungedName << "\n";
  //
  cout << "\tpopl    %ebp\n";
  cout << "\tret\n";
}

/*******************************************************
 *                    AST_Param
 *******************************************************/
void AST_Param::encode()
{
  
}

/*******************************************************
 *                    AST_Param
 *******************************************************/
void AST_Delete::encode()
{
  cout << "\n#\tdelete\n";
  
  cout << "\n#\texpression of delete\n";
  deleteExp->encode();
  
  cout << "\tpushl $" <<line << "\n";
  cout << "\tcall  RTS_checkForNullReference\n";
  cout << "\tpopl  %ecx\n"; 
  cout << "\tpopl  %eax \n"; 
  cout << "\tpushl %eax\n";
  cout << "\tmovl  (%eax), %eax \n"; 
  cout << "\taddl  $4, %eax \n"; 
  cout << "\tmovl  (%eax), %eax \n";
  cout << "\tcall  *%eax\n";
  cout << "\tcall  free\n"; 
  cout << "\taddl  $4, %esp\n";

}

/*******************************************************
 *                    AST_Super
 *******************************************************/
void AST_Super::encode()
{
  cout << "\n#\tsuper\n";
  cout << "\tmovl  8(%ebp), %eax\n";
  cout << "\tpushl %eax\n";
}

/*******************************************************
 *                    AST_MethodCall
 *******************************************************/
void AST_MethodCall::encode()
{
  
  cout << "\n#\tMethod Call\n";
  
  //encode left expression
  leftExp->encode();
  
  //encode arguments
  AST_List* list = argList;
  int n = 0;
  while(list != NULL)
  {
    AST_Expression* exp = (AST_Expression*)list->item;
    exp->encode();
    list = list->restOfList;
    n++;
  }
  
  cout << "\tpushl $" << n+1 << "\n";
  cout << "\tcall  RTS_reverseArgumentsOnStack\n";
  cout << "\tpopl  %ecx\n";
  cout << "\tpushl $" << line << "\n";
  cout << "\tcall  RTS_checkForNullReference\n";
  cout << "\tpopl  %ecx\n";
  cout << "\tpopl  %eax\n";
  cout << "\tpushl %eax\n";
  cout << "\tmovl  (%eax), %eax\n";
  cout << "\taddl  $" << offset << ", %eax\n";
  cout << "\tmovl  (%eax), %eax\n";
  //call by munged name when left exp is "super";
  if(mungedName != "")
  {
    cout << "\tcall " << mungedName << "\n";
  }
  else
  {
    cout << "\tcall  *%eax\n";
  }
  cout << "\taddl  $" << (n+1)*4 << ", %esp\n";
  cout << "\tpushl %eax\n";
}




