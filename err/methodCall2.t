class A{int i;}
class B extends A{}
class C
{
  A a;
  int i;
  int f(A a1, A a2)
  {
    return 1;
  }
  
  int f(A a, B b)
  {
  return 3;
  }
  
  int f(B b, A a)
  {
    return 4;
  }
  
}

int main()
{
  A a;
  B b;
  C c;
  a = new A();
  b = new B();
  c = new C();
  out c.f(a, a);
  out c.f(a, b);
  out c.f(b, a);
  out c.f(b, b);
  out c.f(null, null);
 
   
}
