class A
{
  A(A a, A b)
  {
  }
  A(A a)
  {
    this();
  }
  A()
  {
    this(10, 5);
  }  
  A(int k)
  {
   this();
  }
  A(int i, int j)
  {
    this(10);
  }
}


int main()
{
  A a;
  a = new A();
}
