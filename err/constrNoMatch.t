int main()
{
}

class A
{
  A(int i){}
}

class B extends A
{
}

class C extends A
{
  C()
  {
  }
  C(int i)
  {
    super();
  }
  C(C c)
  {
    super(c);
  }
}
