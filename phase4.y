%{
  // additional productions from the T grammar required for Phase 4
%}

%token THIS SUPER DELETE

%%

ClassBodyDeclaration
	: ClassMemberDeclaration
	| ConstructorDeclaration
	| DestructorDeclaration
	| ';'
	;

ClassMemberDeclaration
	: FieldDeclaration
	| MethodDeclaration
	;

MethodDeclaration
	: Type MethodDeclarator MethodBody
	;

MethodDeclarator
	: Identifier FormalParameters
	;

MethodBody
	: Block
	;

ConstructorDeclaration
	: ConstructorDeclarator ConstructorBody
	;

ConstructorDeclarator
	: Identifier FormalParameters
	;

ConstructorBody
	: '{' ConstructorInvocation BlockStatements '}'
	| '{' ConstructorInvocation '}'
	| Block
	;

ConstructorInvocation
	: THIS Arguments ';'
	| SUPER Arguments ';'
	;

DestructorDeclaration
	: DestructorDeclarator DestructorBody
	;

DestructorDeclarator
	: '~' Identifier '(' ')'
	;

DestructorBody
	: Block
	;

FormalParameters
	: '(' FormalParameterList ')'
	| '(' ')'
	;

FormalParameterList
	: FormalParameterList ',' FormalParameter
	| FormalParameter
	;

FormalParameter
	: Type VariableDeclaratorID
	;

VariableDeclaratorID
	: Identifier
	;

Statement
	: Block
	| EmptyStatement
	| ExpressionStatement
	| IfThenElseStatement
	| WhileStatement
	| ReturnStatement
	| DeleteStatement
	| OutputStatement
	| BreakStatement
	| ContinueStatement
	;

DeleteStatement
	: DELETE Expression ';'
	;

StatementExpression
	: Assignment
	| MethodInvocation
	;

PrimaryNoNewArray
	: ParenExpression
	| THIS
	| FieldAccess
	| MethodInvocation
	| ClassInstanceCreationExpression
	| Literal
	;

FieldAccess
	: Primary '.' Identifier
	| SUPER '.' Identifier
	;

MethodInvocation
	: Identifier Arguments
	| Primary '.' Identifier Arguments
	| SUPER '.' Identifier Arguments
	;

Arguments
	: '(' ArgumentList ')'
	| '(' ')'
	;

ArgumentList
	: ArgumentList ',' Expression
	| Expression
	;

%%
