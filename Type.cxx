// May 2013
// Type representation for T language
//
// NOTE: Don't use Type methods directly. Rather use indirectly via
//       TypeModule. See Type.h for more information.
#define WORD_LEN 4

#include "stdio.h"
#include "Type.h"
#include "AST.h"

extern TypeModule* types;


Type::Type()
{
  name = "";
}

Type::~Type()
{
}

bool Type::isCastableTo(Type* to)
{
  Type* from = this;
  
  //error type
  if(from == types->errorType() || to == types->errorType())
  {
    return false;
  }
  else if(from == to)
  {
    return true;
  }
  //int type
  else if(from == types->intType() || to == types->intType())
  {
    return false;
  }
  //null type
  else if(from == types->nullType() || to == types->nullType())
  {
    return from == types->nullType();
  }
  //class type
  else
  {
    TypeClass* cFrom = (TypeClass*)from;
    TypeClass* cTo = (TypeClass*)to;
    return cFrom->isSubclassOf(cTo) || cTo->isSubclassOf(cFrom);
  }
  
  return false;
}

bool Type::isAssignableTo(Type* to)
{
  Type *from = this;
  //error type
  if(from == types->errorType() || to == types->errorType())
  {
    return false;
  }
  else if(from == to)
  {
    return true;
  }
  //int type
  else if(from == types->intType() || to == types->intType())
  {
    return false;
  }
  //null type
  else if(from == types->nullType() || to == types->nullType())
  {
    return from == types->nullType();
  }
  //class type
  else
  {
    TypeClass* cFrom = (TypeClass*)from;
    TypeClass* cTo = (TypeClass*)to;
    return cFrom->isSubclassOf(cTo);
  }
}



TypeNone::TypeNone()
{
  name = "no-type";
}

TypeNone::~TypeNone()
{
}

char* TypeNone::toString()
{
  return (char *) "<no-type>";
}

TypeError::TypeError()
{
  name = "error-type";
}

TypeError::~TypeError()
{
}

char* TypeError::toString()
{
  return (char *) "<error-type>";
}

TypeInt::TypeInt()
{
  name = "int";
}

TypeInt::~TypeInt()
{
}

char* TypeInt::toString()
{
  return (char *) "<int>";
}

TypeNull::TypeNull()
{
  name = "null";
}

TypeNull::~TypeNull()
{
}

char* TypeNull::toString()
{
  return (char *) "<null>";
}

Method::Method(string cName)
{
  className = cName;
  astConstrDecl = NULL;
}

void Method::addArg(string name, Type* type)
{
  argNameList.push_back(name);
  argTypeList.push_back(type);
}

bool Method::isParamExist(string name)
{
  for (unsigned int i=0; i<argNameList.size(); i++)
  {
    if(argNameList[i] == name)
    {
      return true;
    }
  }
  
  return false;
}

string Method::toString()
{
  string str = "";
  for(unsigned int i=0; i<argNameList.size(); i++)
  {
    str += string(argTypeList[i]->toString()) + " " + argNameList[i] + ",";
  }
  //get rid of the last coma
  if(str.size() > 0)
  {
    str.resize(str.size() - 1);
  }
  
  str =  string(type->toString()) + " " + name + "(" + str + ")" ;
  return str;
  
}

string Method::createMungedName()
{
  mungedName = className;
  string methodName = name;
  
  
  //add a '$' when it is constructor or destructor
  if(type == types->noType())
  {
    mungedName += "$";
  }
  
  //replace '~' with '$' when it is destructor
  if(methodName[0] == '~')
  {
    methodName[0] = '$';
  }
  
  mungedName += "$" + methodName;
  
  for(unsigned int i=0; i<argTypeList.size(); i++ )
  {
    mungedName += "$" + argTypeList[i]->name;
  }
  return mungedName;
}

TypeClass::TypeClass(string cName)
{
  name = cName;
  className = cName;
  isDeclared = false;
  isError = false;
  isVMTCreated = false;
  destructor = NULL;
  
  if(name == "Object")
  {
    parenClass = NULL;
  }
  else
  {
    parenClass =(TypeClass*) types->getOrCreateClassType("Object");
  }
  
}

TypeClass::~TypeClass()
{
}

int TypeClass::getfieldsNumIncludeSuper(TypeClass* classType)
{
  if(classType == NULL || classType->isError || !classType->isDeclared)
  {
    return 0;
  }
  return classType->fieldsList.size() +
         getfieldsNumIncludeSuper(classType->parenClass);
}


//not use
void TypeClass::updateFieldsOffset()
{
  int fieldsNumOfSuper = getfieldsNumIncludeSuper(parenClass);
  map<string, int>::iterator it = fieldsOffset.begin();
  for(; it != fieldsOffset.end(); it++)
  {
    it->second += fieldsNumOfSuper * WORD_LEN;
  }
}

int TypeClass::getFieldOffset(string name)
{
  TypeClass* cur = this;
  while(cur != NULL && !cur->isError)
  {
    if(cur->fieldsList.find(name) != cur->fieldsList.end())
    {
      return getfieldsNumIncludeSuper(cur->parenClass) * WORD_LEN + cur->fieldsOffset[name];
    }
    cur = cur->parenClass;
  }
  return -1;
}

Type* TypeClass::getFieldType(string name)
{
  TypeClass* cur = this;
  while(cur != NULL && !cur->isError)
  {
    if(cur->fieldsList.find(name) != fieldsList.end())
    {
      return cur->fieldsList[name];
    }
    cur = cur->parenClass;
  }

  return NULL;
}

bool TypeClass::isMethodExist(Method *m)
{
  for(unsigned int i=0; i<methodsList.size(); i++)
  {
    if(m->mungedName == methodsList[i]->mungedName)
    {
      return true;
    }
  }  
  return false;
}

bool TypeClass::isConstrExist(Method *m)
{
  for(unsigned int i=0; i<constrList.size(); i++)
  {
    if(m->mungedName == constrList[i]->mungedName)
    {
      return true;
    }
  }
  return false;
}

bool TypeClass::isSubclassOf(TypeClass* cType)
{
  if(this->isError || cType == NULL || cType->isError)
  {
    return false;
  }
  else
  {
    TypeClass* cur = this->parenClass;
    while(cur != NULL && !cType->isError)
    {
      if(cur == cType)
      {
        return true;
      }
      
      cur = cur->parenClass;
    }
    
    return false;
  }
}

void TypeClass::createVMT()
{
  createVMT(this);
}

void TypeClass::createVMT(TypeClass* cType)
{
  if(cType == NULL || cType->isVMTCreated )
  {
    return;
  }
  
  unsigned int parenVMTSize = 0;
  //no super class: Object
  if(cType->parenClass == NULL)
  {
    //reserve for pointer of super VMT
    cType->vmt.push_back(NULL);
    //reverve for destructor
    cType->vmt.push_back(NULL);
  }
  else
  {
    //finish the vmt of super class first
    createVMT(cType->parenClass);
    parenVMTSize = cType->parenClass->vmt.size();
    //copy vmt from super class
    for (unsigned int i=0; i<parenVMTSize; i++)
    {
      cType->vmt.push_back(cType->parenClass->vmt[i]);
    }
  }

  //overide the destructor
  destructor->offset = WORD_LEN;
  cType->vmt[1] = destructor;
  
  //add method of this class
  for(unsigned int i=0; i<cType->methodsList.size(); i++)
  {
    bool isOverriding = false;
    //check if the method is overriding the super class's method
    for (unsigned int j=1; j<parenVMTSize; j++)
    {
      string mungedName1 = getMungedNameWithoutClass(cType->vmt[j]->mungedName);
      string mungedName2 = getMungedNameWithoutClass(cType->methodsList[i]->mungedName);
      
      //overriding
      if(mungedName1 == mungedName2)
      {
        //issue error when the type are the same
        if(cType->vmt[j]->type != cType->methodsList[i]->type)
        {
          cType->methodsList[i]->astConstrDecl->
             printErr("return type must be same with the parent's method when overriding!");
        }
        cType->vmt[j] = cType->methodsList[i];
        cType->vmt[j]->offset = j * WORD_LEN;
        isOverriding = true;
        break;
      }
    }
    //add the mungedName of the method to the vmt if it is not overiding
    if(isOverriding == false)
    {
      int idx = cType->vmt.size();
      cType->vmt.push_back(cType->methodsList[i]);
      cType->vmt[idx]->offset = idx * WORD_LEN;
    }
  }
  cType->isVMTCreated = true;
}

string TypeClass::getMungedNameWithoutClass(string name)
{
  return name.substr(name.find_first_of('$') + 1);
}

Method* TypeClass::getMostSpecMethod(string mType, string name, vector<Type*> args)
{
  vector<Method*> *mList;
  vector<Method*> appList;
  vector<Method*> maxSpecList;

  if(mType == "m")
  {
    mList = &vmt;
  }
  else
  {
    mList = &constrList;
  }
  //step 1: find applicable methods
  for(unsigned int i=0; i<mList->size(); i++)
  {
    Method *m = (*mList)[i];
    if(m == NULL)
    {
      continue;
    }
    //a) name and number of args are same
    if(m->name == name && args.size() == m->argTypeList.size())
    {
      bool applicable = true;
      //b) check if all args is assignable to pars
      for(unsigned int j=0; j<args.size(); j++)
      {
        if(!args[j]->isAssignableTo(m->argTypeList[j]))
        {
          applicable = false;
          break;
        }
      }
      // add applicable method to list
      if(applicable)
      {
        appList.push_back(m);
      }
    }
  }
  
  //step 2: find maximally specific methods
  //(only when there are more than 1 applicable methods)
  
  if(appList.size() == 0)
  {
    return NULL;
  }
  else if(appList.size() == 1)
  {
    return appList[0];
  }
  else
  {
    for (unsigned int i=0; i<appList.size(); i++)
    {
      Method* m1 = appList[i];
      bool hasMoreSpec = false;
      
      for(unsigned int j=0; j<appList.size(); j++)
      {
        Method* m2 = appList[j];
        //check if m2 is more specific than m1
        if(i != j)
        {
          bool moreSpec = true;
          for(unsigned int k=0; k<m1->argTypeList.size(); k++)
          {
            if(!m2->argTypeList[k]->isAssignableTo(m1->argTypeList[k]))
            {
              moreSpec = false;
              break;
            }
          }
          
          //m2 is more specific than m1
          if(moreSpec)
          {
            hasMoreSpec = true;
            break;
          }
        }
      }
      
      //m1 is maximally specific if there is no more specific method
      if(!hasMoreSpec)
      {
        maxSpecList.push_back(m1);
      }
    }
    
    //found the most specific method only if there is one maximally specific method
    if(maxSpecList.size() == 1)
    {
      return maxSpecList[0];
    }
    else
    {
      return NULL;
    }
  }
  
  return NULL;
}

char* TypeClass::toString()
{
  return (char *)("<class-" + className + ">").c_str();
}


void TypeModule::Initialize()
{
  //create predefined class "Object"
  TypeClass* Object = new TypeClass("Object");
  Object->isDeclared = true;
  classTypeList["Object"] = Object;
    
  //create methods for Object class
  // 1. constructor
  Method* m = new Method("Object");
  m->name = "Object";
  m->type = noTypeInternal;
  m->createMungedName();
  Object->constrList.push_back(m);
  
  // 2. destructor
  m = new Method("Object");
  m->name = "~Object";
  m->type = noTypeInternal;
  m->createMungedName();
  Object->destructor = m;
  
  // 3. equals
  m = new Method("Object");
  m->name = "equals";
  m->type = intTypeInternal;
  m->addArg("obj", Object);
  m->createMungedName();
  Object->methodsList.push_back(m);
  
  //create VMT for Object class
  Object->createVMT();

}

TypeModule::TypeModule()
{
  intTypeInternal = (Type*) new TypeInt();
  nullTypeInternal = (Type*) new TypeNull();
  errorTypeInternal = (Type*) new TypeError();
  noTypeInternal = (Type*) new TypeNone();
 
}

TypeModule::~TypeModule()
{
  delete intTypeInternal;
  classTypeList.clear();
  delete errorTypeInternal;
  delete noTypeInternal;
}

Type* TypeModule::intType()
{
  return intTypeInternal;
}

Type* TypeModule::nullType()
{
  return nullTypeInternal;
}

TypeClass* TypeModule::getOrCreateClassType(string name)
{
  if(classTypeList[name] == NULL)
  {
    classTypeList[name] = new TypeClass(name);
  }
  return classTypeList[name];
}

Type* TypeModule::errorType()
{
  return errorTypeInternal;
}

Type* TypeModule::noType()
{
  return noTypeInternal;
}

void TypeModule::updateInfo()
{
  map<string, TypeClass*>::iterator it=classTypeList.begin();
  
  //iterate all classes
  for (; it!=classTypeList.end(); it++)
  {
    TypeClass *cType = it->second;
    if(cType->isDeclared && !cType->isError)
    cType->createVMT();
  }
}

void TypeModule::dumpClasses()
{
  map<string, TypeClass*>::iterator it=classTypeList.begin();
  
  //iterate all classes
  for (; it!=classTypeList.end(); it++)
  {
    string className = it->first;
    TypeClass *cType = it->second;
    
    //only dump classes that are declared
    if(cType->isDeclared)
    {
      cerr << endl;
      cerr << "========================================================" << endl;
      cerr << "             class " << className;
      if(cType->parenClass != NULL)
      {
        cerr << " extends " << cType->parenClass->className;
      }
      cerr << endl;
      cerr << "========================================================" << endl;
      
      //class fields
      cerr << "  fields:" << endl;
      map<string, Type*>::iterator it2 = cType->fieldsList.begin();
      for(; it2 != cType->fieldsList.end(); it2++)
      {
        string fieldName = it2->first;
        Type* type = it2->second;
        cerr << "    " << fieldName << " " << type->toString() << endl;
      }
      cerr << endl;
      
      //class constructors
      cerr << "  constructors:" << endl;
      for(unsigned int i=0; i < cType->constrList.size(); i++)
      {
        Method* m = cType->constrList[i];
        cerr << "    " << m->toString() << endl;
      }
      cerr << endl;
      
      //class destructors
      cerr << "  destructor:" << endl;
      cerr << "    " << cType->destructor->toString() << endl;
      cerr << endl;
      
      //class methods
      cerr << "  methods(prototype):" << endl;
      
      for(unsigned int i=0; i < cType->methodsList.size(); i++)
      {
        Method* m = cType->methodsList[i];
        cerr << "    " << m->toString() << endl;
      }
      cerr << endl;
      
      //class methods
      cerr << "  methods(for VMT):" << endl;
      cerr << "    -------------------------------" << endl;
      cerr << "    idx\t| off\t| MungedName" << endl;
      cerr << "    -------------------------------" << endl;
      for(unsigned int i=1; i < cType->vmt.size(); i++)
      {
        cerr << "    " << i << "\t| "
                       << cType->vmt[i]->offset << "\t| "
                       << cType->vmt[i]->mungedName << endl;
        cerr << "    -------------------------------" << endl;
      }
      //cerr << "========================================================" << endl;
    }
    
  }
  
}

