////////////////////////////////////////////////////////////////////////////
//                              analyze.cxx
//
// Name  : AST analysis for the T language compiler
// Phase : 4
// Date  : May 2013
// Author: Xiaobo Sun
//
// Discription:
//  this file defines the AST methods for analysis
//
////////////////////////////////////////////////////////////////////////////

#define WORD_LEN 4
#define PREFIX_DUP_CLASS "@"

#include <iostream>
using namespace std;
#include <list>
#include <algorithm>

#include "AST.h"
#include "SymbolTable.h"
#include "Type.h"

// global symbol table is in main.cxx
extern SymbolTable* symbolTable;

// global type module is in main.cxx
extern TypeModule* types;

//global type for class
TypeClass* gCurClassType = NULL;
Method* gCurMethod = NULL;
bool gConstrInvoking = false;

void AST_Node::preAnalyze1()
{
}

void AST_Node::preAnalyze2()
{
}

void AST_List::preAnalyze1()
{  
   item->preAnalyze1();
  
   if(restOfList != NULL)
   {
     restOfList->preAnalyze1();
   }
}

/*******************************************************
 *                       PreAnalyze2
 * Scan class declarations to find fields
 *******************************************************/
void AST_List::preAnalyze2()
{
  item->preAnalyze2();
  
  if(restOfList != NULL)
  {
    restOfList->preAnalyze2();
  }
}

/*******************************************************
 *                      AST_List
 *******************************************************/
AST_Node* AST_List::analyze()
{
  item = item->analyze();
  if (restOfList != NULL) restOfList = (AST_List*) restOfList->analyze();
  return (AST_Node*) this;
}

/*******************************************************
 *                      AST_BinaryOperator
 *******************************************************/
AST_Node* AST_BinaryOperator::analyze()
{
  // analyze both subtrees
  left = (AST_Expression*) left->analyze();
  right = (AST_Expression*) right->analyze();
  
  //error type
  if ((left->type == types->errorType()) || (right->type == types->errorType()))
  {
    type = types->errorType();
  }
  //default beheavior: int type
  //operaters that allow other types need to be overwriten(ex: =, ==)
  else if ((left->type != types->intType()) || (right->type != types->intType()))
  {
    printErr("the operands must be int type!");
    type = types->errorType();
  }
  else
  {
    type = left->type;
  }
  
  return (AST_Node*) this;
}

/*******************************************************
 *                      AST_EqualityExpression
 *******************************************************/
AST_Node* AST_EqualityExpression::analyze()
{
  // analyze both subtrees
  left = (AST_Expression*) left->analyze();
  right = (AST_Expression*) right->analyze();
  
  //check error
  if ((left->type == types->errorType()) || (right->type == types->errorType()))
  {
    type = types->errorType();
  }
  else if(isCastable(left->type, right->type) || isCastable(right->type, left->type))
  {
    type = types->intType();
  }
  else
  {
    printErr("Cannot compare expressions: incompatible Types");
    type = types->errorType();
  }
  return (AST_Node*)this;
}

/*******************************************************
 *                      AST_UnaryOperator
 *******************************************************/
AST_Node* AST_UnaryOperator::analyze()
{
  // analyze both subtrees
  left = (AST_Expression*) left->analyze();
  
  // check for error in either subtree to avoid cascade of errors
  if (left->type == types->errorType())
  {
    type = types->errorType();
  }
  else if(left->type != types->intType())
  {
    printErr("Operand need to be int type!");
    type = types->errorType();
  }
  else
  {
    type = left->type;
  }
  return (AST_Node*) this;
}


/*******************************************************
 *            AST_IntegerLiteral
 *******************************************************/
AST_Node* AST_IntegerLiteral::analyze()
{
  //check the value: accept 2^31 only if it is negtive
  if(!isChildOfNegate && value == 0x80000000)
  {
    cerr << "line " << line << ": integer " << value << " is out of range!\n";
    type = types->errorType();
    errCount++;
  } 
  return (AST_Node*) this;
}

/*******************************************************
 *            AST_Variable
 *******************************************************/
AST_Node* AST_Variable::analyze()
{
  //in main block
  if(gCurClassType == NULL)
  {
    Type* typeFromSymbolTable;

    if (symbolTable->lookup(name, typeFromSymbolTable))
    {
      type = typeFromSymbolTable;
      varScope = "main";
    }
    else
    {
      cerr << "line " << line << ": variable " << name << " is not declared!\n";
      type = types->errorType();
      errCount++;
    }
  }
  //in method of class
  else if(gCurMethod != NULL)
  {
    bool found = false;
    //search in parameters
    for(unsigned int i=0; i<gCurMethod->argNameList.size(); i++)
    {
      if(gCurMethod->argNameList[i] == name)
      {
        varScope = "param";
        varPosOrOffset = i;
        type = gCurMethod->argTypeList[i];
        found = true;
        break;
      }
    }
    //search fields
    if(!found)
    {
      int offset = gCurClassType->getFieldOffset(name);
      if(offset >= 0)
      {
        varScope = "field";
        varPosOrOffset = offset;
        type = gCurClassType->getFieldType(name);
        found = true;
        
        if(gConstrInvoking)
        {
          printErr("Can not use fields or methods of the current class as argument of constructor invocation!");
          type = types->errorType();
        }
      }
    }
    
    if(!found)
    {
      printErr("variable " + string(name) + " is neither a field nor a parameter!");
      type = types->errorType();
    }
  }

  // always put Deref node on top of a variable
  AST_Expression* ret = (AST_Expression*) new AST_Deref(this);
  ret->type = type;

  return (AST_Node*) ret;
}

/*******************************************************
 *            AST_MainVarDeclStmt
 *******************************************************/
AST_Node* AST_MainVarDeclStmt::analyze()
{
  //check if the type is declared when it is a class type
  TypeClass* cType = dynamic_cast<TypeClass*>(type);

  if(cType!=NULL && cType->isDeclared == false)
  {
    printErr("unknown type: " + cType->className);
    varList->type = types->errorType();
  }
  else
  {
    //pass the type to AST_IDList
    varList->type = type;
  }
  //analyze the AST_IDList
  varList = (AST_IDList*)varList->analyze();
  
  return (AST_Node*) this;
}

/*******************************************************
 *            AST_MainBlock
 *******************************************************/
AST_Node* AST_MainBlock::analyze()
{
  gCurClassType = NULL;
  gCurMethod = NULL;
  
  if(stmtList != NULL)
  {
    stmtList = (AST_StatementList*)stmtList->analyze();
  }
  return (AST_Node*) this;
}

/*******************************************************
 *            AST_Assignment
 *******************************************************/
AST_Node* AST_Assignment::analyze()
{
  // analyze the lhs variable - a Deref node will be put on top
  AST_Deref* deref = (AST_Deref*) left->analyze();
  // strip off the Deref node
  left = deref->left;
  deref->left = NULL;
  delete deref;

  // analyze the expression
  right = (AST_Expression*) right->analyze();

  //error type
  if ((left->type == types->errorType()) || (right->type == types->errorType()))
  {
    type = types->errorType();
  }
  else if(!isAssignable(right->type, left->type))
  {
    printErr("Cannot assign the expression to left: incompatible types!");
    type = types->errorType();
  }
  else
  {
    type = left->type;
  }

  return (AST_Node*) this;
}

/*******************************************************
 *            AST_Out
 *******************************************************/
AST_Node* AST_Out::analyze()
{
  // analyze the expression
  exp = (AST_Expression*) exp->analyze();
  if(exp->type != types->intType() && exp->type != types->errorType())
  {
    printErr("type error: expression in out statement need to be int type!");
  }
  return (AST_Node*) this;
}

/*******************************************************
 *            AST_ExpressionStmt
 *******************************************************/
AST_Node* AST_ExpressionStmt::analyze()
{
  expStmt =(AST_Expression*) expStmt->analyze();
  return (AST_Node*) this;
}

/*******************************************************
 * The following class are using the analyze of AST_BinaryOperator
 *
 * AST_LessThan AST_GreaterThan
 * AST_Add AST_Sub AST_Mul AST_Div are 
 *******************************************************/


/*******************************************************
 *            AST_Deref
 *******************************************************/
AST_Node* AST_Deref::analyze()
{
  // deref node is added in analyze but should never be analyzed itself
  cerr << "line " << line << ": BUG in AST_Deref::analyze: should never be called\n";

  return (AST_Node*) this;
}

/*******************************************************
 *            AST_IDList
 *******************************************************/
void AST_IDList::preAnalyze2()
{
  //pass the type to AST_ID
  AST_ID* id = (AST_ID*)item;
  
  id->type = type;
  
  
  //analyze the AST_ID
  id->preAnalyze2();
  //analyze the rest of list
  if(restOfList != NULL)
  {
    AST_IDList* idList = (AST_IDList*)restOfList;
    idList->type = type;
    restOfList->preAnalyze2();
  }
}

AST_Node* AST_IDList::analyze()
{
  //pass the type to AST_ID
  AST_ID* id = (AST_ID*)item;
  
  
  id->type = type;
  
  
  //analyze the AST_ID
  item = item->analyze();
  //analyze the rest of list
  if(restOfList != NULL)
  {
    AST_IDList* idList = (AST_IDList*)restOfList;
    idList->type = type;
    restOfList = (AST_List*) restOfList->analyze();
  }
  
  return (AST_Node*) this;
}

/*******************************************************
 *            AST_ID
 *******************************************************/
void AST_ID::preAnalyze2()
{

  if(gCurClassType != NULL)
  {
    if(gCurClassType->fieldsList[name] == NULL)
    {
      gCurClassType->fieldsList[name] = type;
      gCurClassType->fieldsOffset[name] =
        gCurClassType->fieldsList.size() * WORD_LEN;
    }
    else
    {
      printErr("duplicate declaration for class field " + string(name));
    }
  }
}

AST_Node* AST_ID::analyze()
{
  if(gCurClassType == NULL)
  {
    // place declaration into the symbol table
    if (!symbolTable->install(name, type))
    {
      cerr << "line " << line << ": duplicate declaration for " << name << endl;
      errCount++;
    }
  }
  
  return (AST_Node*) this;
}

/*******************************************************
 *            AST_Block
 *******************************************************/
AST_Node* AST_Block::analyze()
{
  if(stmtList != NULL)
  {
    stmtList =(AST_StatementList*) stmtList->analyze();
  }
  return (AST_Node*) this;
}

/*******************************************************
 *            AST_IfThenElse
 *******************************************************/
AST_Node* AST_IfThenElse::analyze()
{
  condition = (AST_Expression*) condition->analyze();
  if(condition->type != types->errorType() &&
     condition->type != types->intType())
  {
    printErr("type error: the condition expression for if-statement must be int type!");
  }
  ifStmt = (AST_Statement*)ifStmt->analyze();
  elseStmt = (AST_Statement*)elseStmt->analyze();
  
  return (AST_Node*) this;
}

/*******************************************************
 *            AST_While
 *******************************************************/
AST_Node* AST_While::analyze()
{
  count++;
  condition = (AST_Expression*) condition->analyze();
  if(condition->type != types->intType() && condition->type != types->errorType())
  {
    printErr("type error: the condition expression for while-statement must be int type!");
  }
  whileStmt = (AST_Statement*)whileStmt->analyze();
  count--;

  return (AST_Node*) this;
}

/*******************************************************
 *            AST_Return
 *******************************************************/
AST_Node* AST_Return::analyze()
{
  curMethod = gCurMethod;
  //if it is in main block , or method(not constructor and destructor)
  if(curMethod == NULL || curMethod->type != types->noType() )
  {
    if (returnExp == NULL)
    {
      printErr("missed expression for return!");
    }
    else
    {
      returnExp = (AST_Expression*)returnExp->analyze();
    
      if(returnExp->type == types->errorType())
      {
        
      }
      //main block
      else if(curMethod == NULL)
      {
        //not int type
        if(returnExp->type != types->intType())
        {
          printErr("type error: return type is must be int in main method!");
        }
      }
      //inside class but not inside a method (would not happen)
      else if(gCurClassType == NULL)
      {
        printErr("bug: return statement must be used inside a method!");
      }
      else
      {
        if(!returnExp->type->isAssignableTo(curMethod->type))
        {
          printErr("type error: return type is not consistent with return type of method!");
        }
      }
    }
  }
  //construstor or destructor
  else
  {
    if (returnExp != NULL)
    {
      printErr("can not return anything in a constructor or destructor!");
    }
  }
  return (AST_Node*) this;
}

/*******************************************************
 *            AST_Break
 *******************************************************/
AST_Node* AST_Break::analyze()
{
  if(AST_While::count <= 0)
  {
    cerr << "line " << line << ": break cannot match any while statement.\n";
    errCount++;
  }
  return (AST_Node*) this;
}

/*******************************************************
 *            AST_Continue
 *******************************************************/
AST_Node* AST_Continue::analyze()
{
  if(AST_While::count <= 0)
  {
    cerr << "line " << line << ": continue cannot match any while statement.\n";
    errCount++;
  }
  return (AST_Node*) this;
}
/*******************************************************
 *            AST_Empty
 *******************************************************/
AST_Node* AST_Empty::analyze()
{
  return (AST_Node*) this;
}

/*******************************************************
 *                    AST_ClassDecl
 *******************************************************/
void AST_ClassDecl::preAnalyze1()
{
  //get class type from types if exits or create a new one
  classType = types->getOrCreateClassType(className);
  
  //change class name if it is duplicated in order to analyze inside class
  if(classType->isDeclared)
  {
    printErr("Duplicate declaration for class " + className);
    className = PREFIX_DUP_CLASS + className;
    classType = types->getOrCreateClassType(className);
    return;
  }
  
  //update the super class
  if(superClassType != NULL)
  {
    classType->parenClass = superClassType;
  }
  
  //mark the class as declared
  classType->isDeclared = true;
}

void AST_ClassDecl::preAnalyze2()
{
  gCurClassType = classType;
  
  //check if the super class is declared
  if(superClassType->isDeclared != true)
  {
    printErr("super class " + superClassType->className + " is not declared");
  }
  
  //check if there is a circle declaration
  TypeClass* cType = superClassType;
  string classTrace = className;
  while(cType != NULL)
  {
    classTrace += "->" + cType->className;
    if(cType == classType)
    {
      printErr("class cyclic inheritance: " + classTrace);
      classType->isError = true;
      break;
    }
    else if(cType->isError || !cType->isDeclared)
    {
      classType->isError = true;
      break;
    }
    cType = cType->parenClass;
  }
  
  if(classBody != NULL)
  {
    classBody->preAnalyze2();
  }
  
  //generate default constructor if there is no one
  if(classType->constrList.size() == 0)
  {
    //constructor body with empty block
    AST_ConstrInvoc* superInvoc = new AST_ConstrInvoc("super", NULL);
    superInvoc->line = line;
    AST_List* block = new AST_List(new AST_Block(NULL), NULL);
    AST_List* body = new AST_List(superInvoc, block);
    //signature with no parameter
    AST_MethodSign* sign = new AST_MethodSign(className, NULL);
    //default constructor
    AST_ConstrDecl* constr = new AST_ConstrDecl(sign, body);
    constr->preAnalyze2();
    
    //add constructor to class body
    classBody = new AST_List(constr, classBody);
  }
  
  //generate default destructor if there is no one
  if(classType->destructor == NULL)
  {
    AST_List* body = new AST_List(new AST_Block(NULL), NULL);
    AST_DestrDecl* destr = new AST_DestrDecl(className, body);
    destr->preAnalyze2();
    classBody = new AST_List(destr, classBody);
  }
}

AST_Node* AST_ClassDecl::analyze()
{
    
  //update current ClassType
  gCurClassType = classType;
  
    
  if(classBody != NULL)
  {
    classBody = (AST_List*)classBody->analyze();
  }
  
  //constructor cyclic invocation check
  for(unsigned int i=0; i<classType->constrList.size(); i++)
  {
    AST_ConstrDecl* constr = (AST_ConstrDecl*)(classType->constrList[i]->astConstrDecl);
    constr->checkCyclicConstrInvoc();
  }
   
 return (AST_Node*) this;
}

/*******************************************************
 *                    AST_Cast
 *******************************************************/
AST_Node* AST_Cast::analyze()
{
  right = (AST_Expression*)right->analyze();

  AST_Variable* var = dynamic_cast<AST_Variable*>(left);
  if(var == NULL)
  {
    printErr("Syntax Error: need a class name in cast");
    type = types->errorType();
   // return (AST_Node*) this;
  }
  else
  {
    TypeClass* cType = types->getOrCreateClassType(var->getName());
    //class undeclared
    if(!cType->isDeclared)
    {
      printErr("unknown type: " + cType->className);
      type = types->errorType();
    }
    else if(right->type == types->errorType())
    {
      type = types->errorType();
    }
    else if(!right->type->isCastableTo(cType))
    {
      printErr("cast error: incampatible types!");
      type = types->errorType();
    }
    else
    {
      type = cType;
    }
  }
  
  return (AST_Node*) this;
}

/*******************************************************
 *                    AST_NULL
 *******************************************************/
AST_Node* AST_NULL::analyze()
{
  type = types->nullType();
  return (AST_Node*) this;
}


/*******************************************************
 *                    AST_FieldDeclStmt
 *******************************************************/
void AST_FieldDeclStmt::preAnalyze2()
{
  TypeClass* cType = dynamic_cast<TypeClass*>(type);
  if(cType!=NULL && cType->isDeclared == false)
  {
    printErr("unknown type: " + cType->className);
    fieldList->type = types->errorType();
    //return;
  }
  else if(fieldList != NULL)
  {
    fieldList->type = type;
  }
  
  fieldList->preAnalyze2();

}

AST_Node* AST_FieldDeclStmt::analyze()
{
  return (AST_Node*) this;
}

/*******************************************************
 *                    AST_CreateObject
 *******************************************************/
AST_Node* AST_CreateObject::analyze()
{
  //check if the class is declared
  TypeClass* cType = dynamic_cast<TypeClass*>(type);
  if(cType == NULL || cType->isDeclared != true)
  {
    printErr("class " + cType->className + " is not declared!");
    type = types->errorType();
    
  }
  
  if(argList != NULL)
  {
    argList = (AST_List*)argList->analyze();
  }
  
  //check arguments
  AST_List* list = argList;

  vector<Type*> argList;
  
  while(list != NULL)
  {
    AST_Expression* exp = (AST_Expression*)list->item;
    if(exp->type == types->errorType())
    {
      type = types->errorType();
    }
    
    argList.push_back(exp->type);
    list = list->restOfList;
  }

  if(type != types->errorType())
  {
    Method* m = cType->getMostSpecMethod("c", cType->className, argList);
  
    //field not found
    if(m == NULL)
    {
      printErr("no constructor can be matched in class " + cType->className + " for creating instance!");
      type = types->errorType();
      return (AST_Node*)this;
    }
    
    constrMungedName = m->mungedName;
  }


  return (AST_Node*) this;
}

/*******************************************************
 *                    AST_FieldAccess
 *******************************************************/
AST_Node* AST_FieldAccess::analyze()
{
  left = (AST_Expression*) left->analyze();
  
  if(left->type == types->errorType())
  {
    type = types->errorType();
  }
  else
  {
    //check if the type is declared when it is a class type
    TypeClass* cType = dynamic_cast<TypeClass*>(left->type);
    
    if(cType == NULL || cType->isDeclared == false)
    {
      printErr("class field access error: not a valid class!");
      type = types->errorType();
      //return (AST_Node*)this;
    }
    else if(cType->isError)
    {
      type = types->errorType();
    }
    else
    {
      type = cType->getFieldType(fieldName);
      
      //field not found
      if(type == NULL)
      {
        printErr("field " + fieldName + " cannot be found in class " + cType->className);
        type = types->errorType();
        //return (AST_Node*)this;
      }
      else
      {
        offset = cType->getFieldOffset(fieldName);
      }
    }
  }
  
  // always put Deref node on top of a variable
  AST_Expression* ret = (AST_Expression*) new AST_Deref(this);
  ret->type = type;
  
  return (AST_Node*) ret;
}

/*******************************************************
 *                    AST_This
 *******************************************************/
AST_Node* AST_This::analyze()
{
  if(gCurClassType == NULL)
  {
    printErr("\"this\" cannot be used outside class!");
    type = types->errorType();
  }
  else
  {

    //if inside the arguments of constructor invocation
    if(gConstrInvoking)
    {
      printErr("Can not use fields or methods of the current class as argument of constructor invocation!");
      type = types->errorType();
    }
    else
    {
      type = gCurClassType;
    }
  }
  
  return (AST_Node*) this;
}

/*******************************************************
 *                    AST_MethodDecl
 *******************************************************/
void AST_MethodDecl::preAnalyze2()
{
   //check if the type is declared when it is a class type
  TypeClass* cType = dynamic_cast<TypeClass*>(type);
 
   if(cType!=NULL && cType->isDeclared == false)
  {
    printErr("unknown type: " + cType->className);
    type = types->errorType();
   }
   
  if(methodSign != NULL)
  {
    methodSign->type = type;
    methodSign->preAnalyze2();
  }
  
  //method => this
  AST_MethodSign* mSign = (AST_MethodSign*)methodSign;
  mSign->method->astConstrDecl = this;
  
}

AST_Node* AST_MethodDecl::analyze()
{
  AST_MethodSign* mSign = (AST_MethodSign*)methodSign;
  gCurMethod = mSign->method;
  
   if(methodBody != NULL)
   {
     methodBody = (AST_Statement*)methodBody->analyze();
   }
   
  return (AST_Node*) this;
}

/*******************************************************
 *                    AST_MethodSign
 *******************************************************/
void AST_MethodSign::preAnalyze2()
{
  if(gCurClassType != NULL)
  {
    //create a new method object
    method = new Method(gCurClassType->name);
    method->name = methodName;
    method->type = type;
    gCurMethod = method;
    
    //analyze and add parameters for method
    if(paramList != NULL)
    {
      paramList->preAnalyze2();
    }
    
    method->createMungedName();
    
    //if it is a constructor
    if(type == types->noType())
    {
      //compile error if the name is not same with class
      if(methodName != gCurClassType->name)
      {
        printErr("Construtor must have same name with class!");
        method->type = types->errorType();
      }
      
      //duplication check
      if(gCurClassType->isConstrExist(method))
      {
        printErr("Duplicate constructor declaration!");
        method->type = types->errorType();
      }
      //add the method to constructor list anyway
      gCurClassType->constrList.push_back(method);
    }
    //regular method
    else
    {
      if(gCurClassType->isMethodExist(method))
      {
        printErr("Duplicate method declaration!");
        method->type = types->errorType();
      }
      
      //add the method to the list anyway
      gCurClassType->methodsList.push_back(method);
    }
  }
}

AST_Node* AST_MethodSign::analyze()
{
  return (AST_Node*) this;
}


/*******************************************************
 *                    AST_ConstrDecl
 *******************************************************/
void AST_ConstrDecl::preAnalyze2()
{
  if(constrSign != NULL)
  {
    constrSign->type = types->noType();
    constrSign->preAnalyze2();
  }
  
  //method => this
  AST_MethodSign* mSign = (AST_MethodSign*)constrSign;
  mSign->method->astConstrDecl = this;

  
}

AST_Node* AST_ConstrDecl::analyze()
{
  AST_MethodSign* mSign = (AST_MethodSign*)constrSign;
  gCurMethod = mSign->method;
  
  if(constrBody != NULL)
  {
    constrBody = (AST_List*)constrBody->analyze();
  }
  
  //get the constructor method that is called in constrInvoc
  AST_ExpressionStmt* stmt = (AST_ExpressionStmt*)constrBody->item;
  constrInvoc = (AST_ConstrInvoc*)stmt->expStmt;
  if(constrInvoc->type == types->errorType())
  {
    type = types->errorType();
  }
  
  return (AST_Node*) this;
}

void AST_ConstrDecl::checkCyclicConstrInvoc()
{
  AST_ConstrDecl* constr = this;
  if(constr->type == types->errorType())
  {
    return;
  }
  
  list<AST_ConstrDecl*> cycleList;
  list<AST_ConstrDecl*>::iterator it;
  cycleList.push_back(constr);
  
  //
  string thisOrSuper = constr->constrInvoc->thisOrSuper;
  Method* constrMethod = constrInvoc->method;
  AST_ConstrDecl* nextConstr = (AST_ConstrDecl*)constrMethod->astConstrDecl;
  
  while(thisOrSuper == "this" && nextConstr->type != types->errorType())
  {
    //duplicate check
    it = find(cycleList.begin(), cycleList.end(), nextConstr);
    if(it != cycleList.end())
    {
      (*it)->printErr("cyclic constructor invocation:");
      
      //print the path of cyclic invocation
      for(;it!=cycleList.end(); it++)
      {
        cerr << "    " << (*it)->constrSign->method->mungedName + " -> " << endl;
      }
      cerr << "    " << nextConstr->constrSign->method->mungedName << endl;
      
      //set the type of all constructors inside the circle to error
      for(it=cycleList.begin(); it!=cycleList.end(); it++)
      {
        (*it)->type = types->errorType();
      }
      nextConstr->type = types->errorType();

      return;
    }
    
    cycleList.push_back(nextConstr);
    
    thisOrSuper = nextConstr->constrInvoc->thisOrSuper;
    constrMethod = nextConstr->constrInvoc->method;
    nextConstr = (AST_ConstrDecl*)constrMethod->astConstrDecl;

  }
  
}
/*******************************************************
 *                    AST_ConstrInvoc
 *******************************************************/
AST_Node* AST_ConstrInvoc::analyze()
{
  if(thisOrSuper == "this")
  {
    leftExp = new AST_This();
  }
  else
  {
    leftExp = new AST_Super();
  }
  
  //analyze this or super
  leftExp = (AST_Expression*) leftExp->analyze();
  
  //argument cannot touch fields or methods of the current class
  gConstrInvoking = true;
  //analyze arguments
  if(argList != NULL)
  {
    argList = (AST_List*)argList->analyze();
  }
  gConstrInvoking = false;
  
  if(leftExp->type == types->errorType())
  {
    type = types->errorType();
    return (AST_Node*) this;
  }
  
  //get class type
  TypeClass* cType = (TypeClass*)(leftExp->type);
  methodName = cType->className;
  
  if(cType->isError)
  {
    type = types->errorType();
    return (AST_Node*) this;
  }
  
  //check arguments
  AST_List* list = argList;
  vector<Type*> argList;
  
  while(list != NULL)
  {
    AST_Expression* exp = (AST_Expression*)list->item;
    if(exp->type == types->errorType())
    {
      type = types->errorType();
    }
    
    argList.push_back(exp->type);
    list = list->restOfList;
  }
  
  if(type != types->errorType())
  {
    Method* m = cType->getMostSpecMethod("c", methodName, argList);
    
    //method not found
    if(m == NULL)
    {
      printErr("no constructor prototype can be matched in class " + cType->className + " for constructor invocation!");
      type = types->errorType();
      //return (AST_Node*)this;
    }
    else
    {
      offset = 0;
      type = types->noType();
      mungedName = m->mungedName;
      method = m;
    }
  }

  //add expression statement upon it
  AST_ExpressionStmt* expStmt = new AST_ExpressionStmt(this);
  
  
  return (AST_Node*) expStmt;
}

/*******************************************************
 *                    AST_DestrDecl
 *******************************************************/
void AST_DestrDecl::preAnalyze2()
{
  if(methodName != gCurClassType->name)
  {
    printErr("the name of destructor must be same with class name!");
    type = types->errorType();
    return;
  }
  
  if(gCurClassType->destructor != NULL)
  {
    printErr("deplicate declaration of destructor!");
    type = types->errorType();
    return;
  }
  gCurClassType->destructor = new Method(gCurClassType->name);
  //replace '~' with '$'
  gCurClassType->destructor->name = "~" + methodName;
  gCurClassType->destructor->type = types->noType();
  gCurClassType->destructor->createMungedName();
  
  //update the curent method
  gCurMethod = gCurClassType->destructor;
  
}

AST_Node* AST_DestrDecl::analyze()
{
  //update the curent method
  gCurMethod = gCurClassType->destructor;
  
  superDestrMungedName = gCurClassType->parenClass->destructor->mungedName;
  methodBody->analyze();
   return (AST_Node*) this;
}

/*******************************************************
 *                    AST_Param
 *******************************************************/
AST_Node* AST_Param::analyze()
{
  return (AST_Node*) this;
}

void AST_Param::preAnalyze2()
{
  if(gCurMethod->isParamExist(paramName))
  {
    printErr("the parameter name is duplicate: [" + paramName + "]");
    type = types->errorType();
  }
  gCurMethod->addArg(paramName, type);
}


/*******************************************************
 *                    AST_Delete
 *******************************************************/
AST_Node* AST_Delete::analyze()
{
  deleteExp = (AST_Expression*)deleteExp->analyze();
  if(deleteExp->type == types->intType()||
     deleteExp->type == types->nullType())
  {
    printErr("the expression in delete statement must be reference type(not null)!");
  }
  return (AST_Node*) this;
}

/*******************************************************
 *                    AST_Super
 *******************************************************/
AST_Node* AST_Super::analyze()
{
  if(gCurClassType == NULL)
  {
    printErr("\"super\" cannot be used outside class!");
    type = types->errorType();
  }
  else
  {
    type = gCurClassType->parenClass;
  }
  
  return (AST_Node*) this;
}

/*******************************************************
 *                    AST_MethodCall
 *******************************************************/
AST_Node* AST_MethodCall::analyze()
{
  if(leftExp != NULL)
  {
    leftExp = (AST_Expression*) leftExp->analyze();
  }
  
  if(argList != NULL)
  {
    argList = (AST_List*)argList->analyze();
  }
  
  if(leftExp->type == types->errorType())
  {
    type = types->errorType();
    return (AST_Node*) this;
  }
 
  //check if the type is declared when it is a class type
  TypeClass* cType = dynamic_cast<TypeClass*>(leftExp->type);
  
  if(cType == NULL || cType->isDeclared == false)
  {
    printErr("method call error: not a valid class!");
    type = types->errorType();
    return (AST_Node*)this;
  }
  
  if(cType->isError)
  {
    type = types->errorType();
    return (AST_Node*) this;
  }

  //check arguments
  AST_List* list = argList;
  vector<Type*> argList;
  
  while(list != NULL)
  {
    AST_Expression* exp = (AST_Expression*)list->item;
    if(exp->type == types->errorType())
    {
      type = types->errorType();
    }
    
    argList.push_back(exp->type);
    //mungedName += "$" + exp->type->name;
    list = list->restOfList;
  }

  if(type != types->errorType())
  {
    Method* m = cType->getMostSpecMethod("m", methodName, argList);
    
    //method not found
    if(m == NULL)
    {
      printErr("no method prototype can be matched in class " + cType->className + "!");
      type = types->errorType();
      return (AST_Node*)this;
    }
    
    offset = m->offset;
    type = m->type;
    
    //check if left exp is "super"
    AST_Super* aSuper = dynamic_cast<AST_Super*>(leftExp);
    if(aSuper != NULL)
    {
      mungedName = m->mungedName;
    }
    else
    {
      mungedName = "";
    }
  }
  
  return (AST_Node*) this;
}












