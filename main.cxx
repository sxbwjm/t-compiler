////////////////////////////////////////////////////////////////////////////
//                              MAIN
//
// Name  : main function for the T language compiler
// Phase : 3
// Date  : Apr 2013
// Author: Xiaobo Sun
//
// Discription:
//  It takes two optional parameters: -before and -after
//    -before: dump the AST tree before analyze
//    -after : dump the AST tree after analyze
//
////////////////////////////////////////////////////////////////////////////

#include "SymbolTable.h"
#include "StringPool.h"
#include "Type.h"

#include <iostream>
using namespace std;
#include <stdio.h>
#include <string.h>

// prototype for bison-generated parser
int yyparse();

// prototypes for code generation functions in encode.cxx
void encodeInitialize();
void encodeFinish();

// to enable bison parse-time debugging
#if YYDEBUG
extern int yydebug;
#endif

// global string pool
StringPool* stringPool;

// global symbol table
SymbolTable* symbolTable;

// global type module
TypeModule* types;

bool dumpBefore = false;
bool dumpAfter = false;
bool dumpClasses = false;

bool CheckUsage(int argc, char* argv[]);

/*******************************************************
 *                      main
 *******************************************************/
int main(int argc, char* argv[])
{
  if(!CheckUsage(argc, argv))
  {
    return 1;
  }
  // create a string pool
  stringPool = new StringPool();

  // create a type module
  types = new TypeModule();
  types->Initialize();

  // create a symbol table
  symbolTable = new SymbolTable();

  // set yydebug to 1 to enable bison debugging
  // (preprocessor symbol YYDEBUG must also be 1 here and in parse.yy)
#if YYDEBUG
  yydebug = 1;
#endif

  // generate prologue code
  encodeInitialize();

  // syntax directed compilation!
  yyparse();

  // generate epilogue code
  encodeFinish();

  // cleanup symbol table
  delete symbolTable;

  // cleanup the types
  delete types;

  // cleanup the string pool
  delete stringPool;
}

/*******************************************************
 *                      CheckUsage
 *******************************************************/
bool CheckUsage(int argc, char* argv[])
{
  bool err = false;
  int i = 0;
  
  if(argc >4)
  {
    err = true;
  }
  else
  {
    //get the dump options
    for(i=1; i<argc; i++)
    {
      if(strcmp(argv[i], "-before") == 0)
      {
        dumpBefore = true;
      }
      else if(strcmp(argv[i], "-after") == 0)
      {
        dumpAfter = true;
      }
      else if(strcmp(argv[i], "-classes") == 0)
      {
        dumpClasses = true;
      }
      else
      {
        err = true;
        break;
      }
    }
  }
  
  //handl error
  if(err)
  {
    cerr << "Usage tc [-before] [-after] [-classes] <infile >outfile" << endl;
    return false;
  }
  
  return true;
    
}
