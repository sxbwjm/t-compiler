class B extends A
{
}
class C extends A
{
  C(int i)
  {
  }
  C()
  {
    this(2);
  }
  C(C c)
  {
    super(1,2);
  }
}
class A
{
  int i;
  A()
  {
    out 1;
  }

  A(int i, int j)
  {
    out i + j;
  }
}

int main()
{
  B b;
  C c;
  b = new B();
  c = new C();
  c = new C(0);
  c = new C(null);
}
