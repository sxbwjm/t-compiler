class A
{
  
  A(int k)
  {
    out k;
  }
}


int main()
{
  A a;
  a = new C();
}

class C extends B
{
  C()
  {
    out 3;
  }
  C(int k)
  {
    super(k+1);
    out k;
 
  }
}

class B extends A
{
  B()
  {
    this(10);
    out 2;
  }
  B(int k)
  {    
    super(k+1);
    out k;

  }
}
