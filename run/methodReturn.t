class A
{
  int i;
  int m1()
  {
    i = 1;
    return i;
    return 2;
  }

  A  m2()
  {
    i = 2;
    return this;
  }

  int m3()
  {
    i = 3;
  }

  A m4()
  {
    i = 4;
  }
}

int main()
{
  A a;
  a = new A();
  out a.m1();
  out a.m2().i;
  out a.m3();
  out a.m4() == null;
}
