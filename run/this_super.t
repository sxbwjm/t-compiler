class A
{
  int x;
  int getX()
  {
    return x;
  }
  A()
  {
    x = 17;
  }
}

class B extends A
{
  int x;
  int getX()
  {
    return x;
  }
  int method()
  {
    out super.x;
    out this.x;
    out this.getX();
    out x;
    out getX();
    out super.getX();
  }
  B()
  {
    x = 19;
  }
 
}
class C extends B
{
  int x;
  int getX()
  {
    return x;
  }
  C()
  {
    x = 21;
  }
}

int main()
{
  C c;
  c = new C();
    
  c.method();
  
 
   
}
