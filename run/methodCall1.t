class A{int i;}
class B extends A{}
class C
{
  A a;
  int i;

  int f(int k)
  {
    i = 42;
    out i;
    return i;
  }
  A f(A a)
  {
    a = new A();
    a.i = 5;
    out a.i;
    return a;
  }

  int f()
  {
    out 1;
  }
  
  int f(int j, int k)
  {
    out j + k;
  }

  int f(int i, int j, int k)
  {
    out i+j+k;
  }
  
}

int main()
{
  A a;
  B b;
  C c;
  c = new C();
  
  out c.f();
  out c.f(a).i;
  out c.f(5);
  out c.f(5, 3);
  out c.f(7,2, 1);
   
}
