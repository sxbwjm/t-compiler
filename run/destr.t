class B extends A
{
  ~B(){out 1;}
}
class C extends B
{
}

int main()
{
  B b;
  b = new B();
 delete b;
  C c;
  c = new C();
  delete c;
}

class A
{
   ~A(){out 2;}
}
