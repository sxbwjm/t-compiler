class A
{
  int i;
}
class B extends A
{
  int j;
  A a;
  B()
  {
    j = 1;
    out j;
  }
  
  B(int k)
  {
    j = k;
    out j;
  }
  
  B(A aa)
  {
    j = aa.i;
    a = aa;
    out aa.i;
  }
  
}


int main()
{
  A a;
  B b;

  a = new A();
  a.i = 3;
  b = new B();
  out b.j;
  b = new B(2);
  out b.j;
  b = new B(a);
  out b.j;
  out b.a.i;

   
}
