class A
{
  A()
  {
    super();
    out 1;
  }
  A(int k)
  {
    out k;
  }
}


int main()
{
  A a;
  a = new C();
  a = new C(4); 
}

class C extends B
{
  C()
  {
    out 3;
  }
  C(int k)
  {
    super(k+1);
    out k;
 
  }
}

class B extends A
{
  B()
  {
    out 2;
  }
  B(int k)
  {    
    super(k+1);
    out k;

  }
}
