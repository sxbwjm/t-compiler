#include <stdio.h>
#include <stdlib.h>

void RTS_outputInteger(int i)
{
  printf("%d\n", i);
}

void RTS_printDivideByZeroError(int line)
{
  printf("ERROR: divided by zeor (line %d)\n", line);
  exit(-1);
}

void RTS_outOfMemoryError(int line)
{
  printf("ERROR: Out of memory (line %d)\n", line);
  exit(-1);
}

void RTS_checkForNullReference(int line, int* addr)
{
  if(addr == NULL)
  {
    printf("ERROR: Null reference (line %d)\n", line);
    exit(-1);
  }
}

void RTS_checkCast(int line, int* castType, int* objType)
{
  //do nothing if the object is null
  if(objType == NULL)
  {
    return;
  }
  
  int* cur = (int*)(*objType);
  
  while(cur != NULL)
  {
    if(cur == castType)
    {
      return;
    }
    cur = (int*)(*cur);
  }
  
  printf("ERROR: Invalid cast (line %d)\n", line);
  exit(-1);
  
}

void RTS_reverseArgumentsOnStack(int num)
{
  int* p = &num;
  p++;
  int i = 0;
  for(i=0; i<num/2; i++)
  {
    int tmp = *(p + i);
    *(p + i) = *(p+ num - i - 1);
    *(p+ num - i - 1) = tmp;
  }
}

