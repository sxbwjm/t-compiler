#ifndef _AST_H
#define _AST_H

////////////////////////////////////////////////////////////////////////////
//                              AST.h
//
// Name  : AST declaration for the T language compiler
// Phase : 4
// Date  : May 2013
// Author: Xiaobo Sun
//
// Discription:
//  this file declare the AST node class
//  these classes mainly involve the following functions:
//
//  dump() - methods are for debugging - they display the AST to stderr.
//
//  analyze() - perform semantic analysis
//
//  encode() - perform code generation
//
////////////////////////////////////////////////////////////////////////////

#include "Type.h"
#include <stack>

// abstract class: all AST nodes derived from this
class AST_Node
{
  public:
    int line;

  public:
    virtual ~AST_Node();
 
    virtual void dump() = 0;
  
    virtual void preAnalyze1();
    virtual void preAnalyze2();
    virtual AST_Node* analyze() = 0;
    virtual void encode() = 0;
  
    // set the line number of the node
    virtual void setLineNumber( int );

    // get the line number of the node
    virtual int getLineNumber();
  
    void printErr(string errMsg);
  
    static int errCount;
  

  protected:
    AST_Node();
};

// abstract class: all list nodes derived from this
class AST_List: public AST_Node
{
  public:
    AST_Node* item;
    AST_List* restOfList;
    
  public:
    virtual ~AST_List();
 
    // default behavior for lists: recurse left, then recurse right
    virtual void dump();
    virtual AST_Node* analyze();
    virtual void encode();
    void preAnalyze1();
    void preAnalyze2();
    
  //protected:
    AST_List(AST_Node* newItem, AST_List* list);
    //append list2 to list1
    void append(AST_List* list);
};


// abstract class: all statements derived from this
class AST_Statement: public AST_Node
{
  public:
    virtual ~AST_Statement();
 
  protected:
    AST_Statement();
};

// abstract class: all expression nodes dervived from this
class AST_Expression: public AST_Node
{
  public:
    virtual ~AST_Expression();

    Type* type;

  protected:
    AST_Expression();
};

// abstract class: all binary operation expression nodes derived from this
class AST_BinaryOperator: public AST_Expression
{
  public:
    enum Convertion_Type{NUM, ASSIGN, CAST};
  
    virtual ~AST_BinaryOperator();
    AST_Expression* left;
    AST_Expression* right;
    AST_Node* analyze();
   
    bool CheckTypeConvertion();
    bool isCastable(Type* from, Type* to);
    bool isAssignable(Type* from, Type* to);
    
  protected:
    AST_BinaryOperator(AST_Expression* l, AST_Expression* r);
    Convertion_Type convertionType;
        
  
};

// abstract class: all unary operation expression nodes dervived from this
class AST_UnaryOperator: public AST_Expression
{
  public:
    virtual ~AST_UnaryOperator();
    AST_Expression* left;
    AST_Node* analyze();

  protected:
    AST_UnaryOperator(AST_Expression* l);
};

class AST_ID: public AST_Expression
{
protected:
  char* name;
public:
  ~AST_ID();
  AST_ID(char* s);
  
  void preAnalyze2();
  void dump();
  AST_Node* analyze();
  void encode();
};

class AST_IDList: public AST_List
{
protected:
  
public:
  Type* type;
  void preAnalyze2();
  
  ~AST_IDList();
  AST_IDList(AST_ID* id, AST_List* rest);
  AST_Node* analyze();
  
};

class AST_MainVarDeclStmt: public AST_Statement
{
protected:
  Type* type;
  AST_IDList* varList;
public:

  ~AST_MainVarDeclStmt();
  AST_MainVarDeclStmt(Type* tp, AST_IDList* list);
  
  void dump();
  AST_Node* analyze();
  void encode();
};

// variable
class AST_Variable: public AST_Expression
{
  protected:
    char* name;
  string varScope;
  int varPosOrOffset;

  public:
    ~AST_Variable();
    AST_Variable(char *id);
    string getName();
  
    void dump();
    AST_Node* analyze();
    void encode();
};

// integer literal
class AST_IntegerLiteral: public AST_Expression
{
  protected:
    unsigned int value;
  public:
    ~AST_IntegerLiteral();
    AST_IntegerLiteral(unsigned int in);
    bool isChildOfNegate;

    void dump();
    AST_Node* analyze();
    void encode();
};

// list of statements
class AST_StatementList: public AST_List
{
  public:
    ~AST_StatementList();
    AST_StatementList(AST_Statement* statement, AST_List* restOfList);
 
    // inherit default behavior for dump, analyze, encode
};

//
class AST_MainBlock: public AST_Node
{
protected:
  AST_StatementList* stmtList;
public:
  ~AST_MainBlock();
  AST_MainBlock(AST_StatementList* list);
  
  void dump();
  AST_Node* analyze();
  void encode();
};

// declaration statement
class AST_Declaration: public AST_Statement
{
  public:
    Type* type;
    ~AST_Declaration();
    AST_Declaration();
};

// assignment statement
class AST_Assignment: public AST_BinaryOperator
{
  public:
    ~AST_Assignment();
    AST_Assignment(AST_Expression* l, AST_Expression* r);
    
    void dump();
    AST_Node* analyze();
    void encode();
};

// out statement
class AST_Out: public AST_Statement
{
  protected:
    AST_Expression* exp;

  public:
    ~AST_Out();
    AST_Out(AST_Expression* var);
    
    void dump();
    AST_Node* analyze();
    void encode();
};

// Expression statement
class AST_ExpressionStmt: public AST_Statement
{
public:
  // represent with a AST_Expression because a Deref will be added
  AST_Expression* expStmt;

public:
  ~AST_ExpressionStmt();
  AST_ExpressionStmt(AST_Expression* exp);
  
  void dump();
  AST_Node* analyze();
  void encode();
};

// Equality Expression
class AST_EqualityExpression: public AST_BinaryOperator
{
public:
  ~AST_EqualityExpression();
  AST_EqualityExpression(AST_Expression* left, AST_Expression* right);
  
  void dump();
  AST_Node* analyze();
  void encode();
};


// less than
class AST_LessThan: public AST_BinaryOperator
{
  public:
    ~AST_LessThan();
    AST_LessThan(AST_Expression* left, AST_Expression* right);

    void dump();
    //AST_Node* analyze();
    void encode();
};

// greater than
class AST_GreaterThan: public AST_BinaryOperator
{
  public:
    ~AST_GreaterThan();
    AST_GreaterThan(AST_Expression* left, AST_Expression* right);

    void dump();
    //AST_Node* analyze();
    void encode();
};


// Add
class AST_Add: public AST_BinaryOperator
{
  public:
    ~AST_Add();
    AST_Add(AST_Expression* left, AST_Expression* right);

    void dump();
    //AST_Node* analyze();
    void encode();
};

// Subtract 
class AST_Sub: public AST_BinaryOperator
{
  public:
    ~AST_Sub();
    AST_Sub(AST_Expression* left, AST_Expression* right);

    void dump();
    //AST_Node* analyze();
    void encode();
};

// Multiply 
class AST_Mul: public AST_BinaryOperator
{
  public:
    ~AST_Mul();
    AST_Mul(AST_Expression* left, AST_Expression* right);

    void dump();
    //AST_Node* analyze();
    void encode();
};

// Divide 
class AST_Div: public AST_BinaryOperator
{
  public:
    ~AST_Div();
    AST_Div(AST_Expression* left, AST_Expression* right);

    void dump();
    //AST_Node* analyze();
    void encode();
};

// dereference unary operator
class AST_Deref: public AST_UnaryOperator
{
  public:
    ~AST_Deref();
    AST_Deref(AST_Expression* left);

    void dump();
    AST_Node* analyze();
    void encode();
};

class AST_Negate: public AST_UnaryOperator
{
  public:
    ~AST_Negate();
    AST_Negate(AST_Expression* left);
  
    void dump();
    void encode();
};

class AST_Not: public AST_UnaryOperator
{
  public:
    ~AST_Not();
    AST_Not(AST_Expression* left);
  
    void dump();
    void encode();
};

class AST_Block: public AST_Statement
{
private:
  AST_StatementList* stmtList;
public:
  ~AST_Block();
  AST_Block(AST_StatementList* list);
  
  void dump();
  AST_Node* analyze();
  void encode();
};

class AST_IfThenElse: public AST_Statement
{
private:
  AST_Expression* condition;
  AST_Statement* ifStmt;
  AST_Statement* elseStmt;
  
public:
  static int labelNum;
  ~AST_IfThenElse();
  AST_IfThenElse(AST_Expression* exp,
                 AST_Statement* stmt1,
                 AST_Statement* stmt2);
  
  void dump();
  AST_Node* analyze();
  void encode();
};

class AST_While: public AST_Statement
{
private:
  AST_Expression* condition;
  AST_Statement* whileStmt;
public:
  static int count;
  static stack<unsigned int>  whileStack;
  ~AST_While();
  AST_While(AST_Expression* exp, AST_Statement* stmt);
  
  void dump();
  AST_Node* analyze();
  void encode();
};

class AST_Return: public AST_Statement
{
private:
  AST_Expression* returnExp;
  Method* curMethod;
public:
  ~AST_Return();
  AST_Return(AST_Expression* exp);
  
  void dump();
  AST_Node* analyze();
  void encode();
};

class AST_Break: public AST_Statement
{
public:
  ~AST_Break();
  AST_Break();
  
  void dump();
  AST_Node* analyze();
  void encode();
};

class AST_Continue: public AST_Statement
{
public:
  ~AST_Continue();
  AST_Continue();
  
  void dump();
  AST_Node* analyze();
  void encode();
};

class AST_Empty: public AST_Statement
{
public:
  ~AST_Empty();
  AST_Empty();
  
  void dump();
  AST_Node* analyze();
  void encode();
};

class AST_ClassDecl: public AST_Node
{
private:
  string className;
  AST_List* classBody;
  TypeClass* classType;
  TypeClass* superClassType;
public:
  ~AST_ClassDecl();
  AST_ClassDecl(string name,AST_List* body, Type* superClass);
  
  void preAnalyze1();
  void preAnalyze2();
  
  void dump();
  AST_Node* analyze();
  void encode();
};

class AST_Cast: public AST_BinaryOperator
{
public:
  ~AST_Cast();
  AST_Cast(AST_Expression* l, AST_Expression* r);
  
  void dump();
  AST_Node* analyze();
  void encode();
};

class AST_NULL: public AST_Expression
{
public:
  ~AST_NULL();
  AST_NULL();
  
  void dump();
  AST_Node* analyze();
  void encode();
};

class AST_FieldDeclStmt: public AST_Declaration
{
protected:
  AST_IDList* fieldList;
public:
  ~AST_FieldDeclStmt();
  AST_FieldDeclStmt(Type* tp, AST_List* list);
  
  void preAnalyze2();
  void dump();
  AST_Node* analyze();
  void encode();
};


class AST_CreateObject: public AST_Expression
{
protected:
  //Type* type;
  AST_List* argList;
  string constrMungedName;
public:
  static int labelNum;
  ~AST_CreateObject();
  AST_CreateObject(Type* tp, AST_List* list);
  
  void dump();
  AST_Node* analyze();
  void encode();
};

class AST_FieldAccess: public AST_UnaryOperator
{
protected:
  //AST_Expression* classExp;
  string fieldName;
  int offset;
public:
  ~AST_FieldAccess();
  AST_FieldAccess(AST_Expression* exp, string field);
  
  void dump();
  AST_Node* analyze();
  void encode();
};

class AST_This: public AST_Expression
{
public:
  AST_This();
  ~AST_This();
  
  void dump();
  AST_Node* analyze();
  void encode();
};

class AST_MethodDecl: public AST_Declaration
{
protected:
  AST_Declaration* methodSign;
  AST_Statement* methodBody;
public:
  AST_MethodDecl(Type* tp, AST_Declaration* name, AST_Statement* body);
  ~AST_MethodDecl();
  
  void dump();
  void preAnalyze2();
  AST_Node* analyze();
  void encode();
};

class AST_MethodSign: public AST_Declaration
{
protected:
  string methodName;
  AST_List* paramList;
public:
  Method* method;
  AST_MethodSign(string name, AST_List* params);
  ~AST_MethodSign();
  
  void dump();
  void preAnalyze2();
  AST_Node* analyze();
  void encode();
};


class AST_DestrDecl: public AST_Declaration
{
protected:
  string methodName;
  AST_List* methodBody;
  string superDestrMungedName;
public:
  AST_DestrDecl(string name, AST_List* body);
  ~AST_DestrDecl();
  
  void dump();
  void preAnalyze2();
  AST_Node* analyze();
  void encode();
};

class AST_Param: public AST_Declaration
{
  string paramName;
public:
  AST_Param(Type* tp, string parId);
  ~AST_Param();
  
  void dump();
  void preAnalyze2();
  AST_Node* analyze();
  void encode();
};

class AST_Delete: public AST_Statement
{
protected:
  AST_Expression* deleteExp;
public:
  AST_Delete(AST_Expression* exp);
  ~AST_Delete();
  
  void dump();
  AST_Node* analyze();
  void encode();
};

class AST_Super: public AST_Expression
{
public:
  AST_Super();
  ~AST_Super();
  
  void dump();
  AST_Node* analyze();
  void encode();
};

class AST_MethodCall: public AST_Expression
{
protected:
  AST_Expression* leftExp;
  string methodName;
  AST_List* argList;
  int offset;
  string mungedName;
  bool isConstr;
public:
  AST_MethodCall(AST_Expression* exp, char* name, AST_List* args);
  ~AST_MethodCall();
  
  void dump();
  AST_Node* analyze();
  void encode();
};

class AST_ConstrInvoc: public AST_MethodCall
{
public:
  string thisOrSuper;
  Method* method;
  
  AST_ConstrInvoc(string which, AST_List* args);
  ~AST_ConstrInvoc();
  
  void dump();
  AST_Node* analyze();
  //void encode();
};

class AST_ConstrDecl: public AST_Declaration
{
protected:
  AST_MethodSign* constrSign;
  AST_List* constrBody;
public:
  AST_ConstrDecl(AST_Declaration* name, AST_List* body);
  ~AST_ConstrDecl();
  
  AST_ConstrInvoc* constrInvoc;
  
  void dump();
  void preAnalyze2();
  AST_Node* analyze();
  void encode();
  
  void checkCyclicConstrInvoc();
};

#endif
